/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.ArrayList;

/**
 * @author Quan Bai and Weihua Li
 */
public class B08_Generics {

    public static void main(String args[]) {

        //with Generic
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(new Integer("345"));
        //aList2.add("aaa");  //this is not accepted
        int i = list1.get(0);
        System.out.println(i);

        //without Generic
        ArrayList list2 = new ArrayList();
        list2.add(678);
        int j = (int) list2.get(0);
        System.out.println(j);

    }
}
