/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.*;

/**
 * @author Quan Bai and Weihua Li
 * @Note 1. Hashtable is synchronized, whereas HashMap is not. This makes
 * HashMap better for non-threaded applications, as unsynchronized Objects
 * typically perform better than synchronized ones.
 * @Note 2. Hashtable does not allow null keys or values. HashMap allows one
 * null key and any number of null values.
 * @Note 3. One of HashMap's subclasses is LinkedHashMap, so in the event that
 * you'd want predictable iteration order (which is insertion order by default),
 * you could easily swap out the HashMap for a LinkedHashMap. This wouldn't be
 * as easy if you were using Hashtable.
 */
public class B07_HashMap {

    public static void main(String args[]) {
        Map myMap = new HashMap();

        //Adding elements
        //new Integer(1) is an object, int i =1 is primitive
        myMap.put("One", new Integer(1));
        myMap.put("Two", new Integer(2));
        myMap.put("Three", new Integer(3));
        printoutMap(myMap);

        //Getting elements:
        Integer i = (Integer) myMap.get("One");
        myMap.remove("One");
        printoutMap(myMap);

        //Get all keys -> keys are not duplicated
        Set keySet = myMap.keySet();

        //Get all the values:
        Collection valueCollection = myMap.values();
        for (Object o : valueCollection) {
            System.out.println(o);
        }

        //HashMap to Iterator
        //Get a set of the entries by using entrySet()
        //Each element in the returned set is a Map.Entry (key-value pair)
        Set eSet = myMap.entrySet();
        Iterator it = eSet.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        //You can also split the value pair by using below
        it = eSet.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String key = (String) entry.getKey();
            Integer value = (Integer) entry.getValue();
            System.out.println("Key: " + key + ", Value: " + value);
        }

    }

    public static void printoutMap(Map map) {
        //Concatenate Strings (more efficent than using +)
        StringBuilder sb = new StringBuilder();
        for (Object o : map.keySet()) {
            sb.append("Key: ").append(o.toString()).append(", ");
            sb.append("Value: ").append(map.get(o)).append("\n");
        }
        System.out.println(sb.toString());
    }
}
