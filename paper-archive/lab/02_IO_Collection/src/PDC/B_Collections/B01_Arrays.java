/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

/**
 * @author Quan Bai and Weihua Li
 */
public class B01_Arrays {

    public static void main(String args[]) {
        //Declaring an array
        int[] myArray = new int[5];
        int intMultArray[][] = {{1, 2,}, {2, 3}, {4, 5, 6}}; //multidimension array

        String[] stringArray10 = new String[10];
        String[] stringArray2 = new String[]{"one", "two"};

        //check the length - first approach
        for (int i = 0; i < stringArray10.length; i++) {
            String s = stringArray10[i];
            System.out.println(s);
        }

        //check the length - second approach
        for (String a : stringArray2) {
            String s = a;
            System.out.println(s);
        }

    }
}
