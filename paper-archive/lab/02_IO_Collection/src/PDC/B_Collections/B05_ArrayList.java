/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Quan Bai and Weihua Li
 */
public class B05_ArrayList {

    //1. ArrayList internally uses dynamic array to store elements
    //2. Manipulate with ArrayList is slow because it internally uses array.
    //3. ArrayList class can act as a list only because it implements List only.
    //4. ArrayList is better for storing and accessing data.
    public static void main(String args[]) {
        //Constructor
        List myList = new ArrayList();

        //Adding elements
        myList.add("A");
        myList.add(Integer.valueOf("1"));
        myList.add(1, "C");

        //Replacing an element:
        myList.set(2, "Milan");

        //Getting the elements:
        myList.get(1);
        Object o = myList.get(1);
        String s = (String) myList.get(1);

        //Removing elements
        myList.remove(1);
        myList.remove("A");
        myList.clear();

    }

}
