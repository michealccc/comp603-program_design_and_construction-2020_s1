/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Quan Bai and Weihua Li
 */
public class B02_Iterator {

    //Getting all elements from a collection:
    //For Lists, we could use a for loop, and loop through the list to get() each item
    //But this doesn��t work for Maps.
    //To allow generic handling of collections, Java defines an object called an Iterator
    //An object whose function is to walk through a Collection of objects and provide access to each object in sequence
    //Iterator objects have three methods:
    //next() ? gets the next item in the collection
    //hasNext() ? tests whether it has reached the end
    //remove() ? removes the item just returned
    public static void main(String args[]) {

        List myList = new ArrayList();
        myList.add("PDC");
        myList.add("ASP.NET Web Forms");
        myList.add("ASP.NET Windows Forms");
        myList.add("ASP.NET MVC");

        //loop through the list
        for (int i = 0; i < myList.size(); i++) {
            System.out.println(myList.get(i));
        }

        //use Iterator
        Iterator it = myList.iterator();

        while (it.hasNext()) {
            //If you step into next(), you will find: E next();
            //where the E denotes the type of elements returned by this iterator
            System.out.println((String) it.next());
        }

    }
}
