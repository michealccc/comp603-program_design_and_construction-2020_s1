/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.HashSet;
import java.util.Scanner;

/**
 * @author Quan Bai and Weihua Li
 */
public class B03_HashSet {

    public static void main(String args[]) {
        //Generic
        HashSet<Integer> numbers = new HashSet();
        Scanner in = new Scanner(System.in);
        //Input: 22 33 56 33 65 66 22, x
        System.out.println("Please input the numbers...");
        while (in.hasNextInt()) {
            Integer num = in.nextInt();
            numbers.add(num);
        }

        for (Integer i : numbers) {
            System.out.println(i);
        }

        //This is Lambda Expression, FYI ONLY
        //For more information:
        //https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html
        numbers.stream().forEach((i) -> {
            System.out.println(i);
        });

    }

}
