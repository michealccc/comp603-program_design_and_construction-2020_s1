/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.HashSet;

/**
 * @author Quan Bai and Weihua Li
 */
public class B04_ObjectEquals {

    public static void main(String args[]) {
        Student stuA = new Student(123);
        Student stuB = new Student(123);

        System.out.println(stuA.equals(stuB));
        System.out.println(stuA == stuB);

        HashSet hs = new HashSet();
        hs.add(stuA);
        hs.add(stuB);
        System.out.println(hs.size());

    }

}

class Student {

    int id;

    public Student(int n) {
        this.id = n;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        Student other = (Student) o;
        return (this.id == other.id);
    }

    //You must override hashCode() in every class that overrides equals(). 
    //Failure to do so will result in a violation of the general contract 
    //for Object.hashCode(), which will prevent your class from functioning 
    //properly in conjunction with all hash-based collections, including HashMap, HashSet, and Hashtable.
    @Override
    public int hashCode() {
        int hashCode = 1;
        hashCode = 100 * hashCode + this.id;
        return hashCode;
    }

}
