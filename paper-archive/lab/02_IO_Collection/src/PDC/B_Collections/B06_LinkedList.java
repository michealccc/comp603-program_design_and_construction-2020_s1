/*
 * The programs are designed for PDC paper
 */
package PDC.B_Collections;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Quan Bai and Weihua Li
 */
public class B06_LinkedList {

    //1. LinkedList internally uses doubly linked list to store the elements.
    //2. Manipulation with LinkedList is faster than ArrayList
    //3. LinkedList class can act as a list and queue both
    //4. LinkedList is better for manipulating data. 

    public static void main(String args[]) {

        //Instantiate a list
        List list = new LinkedList();
        for (int i = 0; i < 9; i++) {
            list.add("a" + i);
        }

        //Printout the original
        System.out.println(list);

        //Randmize
        Collections.shuffle(list);
        System.out.println(list);

        //Reverse order -> based on randomized list
        Collections.reverse(list);
        System.out.println(list);

        //Sort the list
        Collections.sort(list);
        System.out.println(list);

        //binary search
        int index = Collections.binarySearch(list, "a6");
        System.out.println("Binary Search:" + index);

    }
}
