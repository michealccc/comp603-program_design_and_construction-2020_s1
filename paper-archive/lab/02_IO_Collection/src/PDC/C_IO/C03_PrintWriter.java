/*
 * The programs are designed for PDC paper
 */
package PDC.C_IO;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * @author Quan Bai and Weihua Li
 */
public class C03_PrintWriter {

    public static void main(String[] args) {
        PrintWriter outputStream = null;
        try {
            //override the file if exists
            outputStream = new PrintWriter(new FileOutputStream("out.txt"));

            //append to the file, rather than override
            //outputStream = new PrintWriter(new FileOutputStream("output.txt", true)); 

            for (int count = 1; count <= 3; count++) {
                outputStream.println("Student No." + count);
            }

            System.out.println("... written to out.txt.");

            //see what happens without closing the outputsream? 
            outputStream.close();

        } catch (FileNotFoundException e) {
            System.out.println("Error opening the file out.txt." + e.getMessage());
        }
    }
}
