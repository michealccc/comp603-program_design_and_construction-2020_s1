/*
 * The programs are designed for PDC paper
 */

package PDC.C_IO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

/**
 * @author Quan Bai and Weihua Li
 */

public class C06_Scanner {

    public static void main(String args[]) {
        String str = "abc,,,,,dec. xyz:txt*****aaa.bbb";
        Scanner sc = new Scanner(str);
        sc.useDelimiter(",,*");
        while (sc.hasNext()) {
            System.out.println(sc.next());
        }

        System.out.println("==============================");

        sc = new Scanner(str);
        sc.useDelimiter(",,*|\\. *|: *|\\*\\**");
        while (sc.hasNext()) {
            System.out.println(sc.next());
        }

        System.out.println("==============================");

        //Scanner is used for parsing tokens from the contents of the 
        //stream while BufferedReader just reads the stream and does not do any special parsing.
        try {
            BufferedReader br = new BufferedReader(new FileReader("sample.txt"));
            Scanner scanner = new Scanner(br);

            while (scanner.hasNextLine()) {
                String s = scanner.nextLine();
                System.out.println(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println("==============================");

    }
}
