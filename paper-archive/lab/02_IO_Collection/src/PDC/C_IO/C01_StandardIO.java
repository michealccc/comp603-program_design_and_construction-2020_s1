/*
 * The programs are designed for PDC paper
 */
package PDC.C_IO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;


/**
 * @author Quan Bai and Weihua Li
 */
public class C01_StandardIO {

    public static void main(String[] args) throws FileNotFoundException {
        PrintStream stream1 = System.out;
        File aFile = new File("sysout.log");
        PrintStream stream2 = new PrintStream(new FileOutputStream(aFile));
        System.out.println("1");
        System.setOut(stream2);
        System.out.println("2");
        System.setOut(stream1);
        System.out.println("3");


    }
}
