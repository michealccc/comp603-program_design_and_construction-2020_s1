/*
 * The programs are designed for PDC paper
 */
package PDC.C_IO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Quan Bai and Weihua Li
 */
public class C02_Streams {

    public static void main(String args[]) {
        //read file byte by byte
        readFileByteByByte();
        //copy file -> read and write
        copyFile();
    }

    public static void readFileByteByByte() {
        int b = 0;
        FileInputStream in = null;
        try {
            in = new FileInputStream("sample.txt");
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found!");
            return;
        }

        try {
            long num = 0;
            //Convert bytes to characters
            while ((b = in.read()) != -1) {
                System.out.print((char) b);
                num++;
            }
            in.close();
            System.out.println();
            //the file size
            System.out.println("Total " + num + " Bytes");
        } catch (IOException ex) {
            System.out.println("Reading Error!");
        }
    }

    public static void copyFile() {
        int b;
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new FileInputStream("sample.jpg");
            out = new FileOutputStream("sample_copy.jpg");

            while ((b = in.read()) != -1) {
                out.write(b);
            }
            in.close();
            out.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found");
            return;
        } catch (IOException ex) {
            System.out.println("Copying File Error!");
            return;
        }
        System.out.println("File has been copied!");
    }
}
