/*
 * The programs are designed for PDC paper
 */
package PDC.C_IO;

import java.util.StringTokenizer;

/**
 * @author Quan Bai and Weihua Li
 */
public class C05_StringTokenizer {

    public static void main(String args[]) {

        //use StringTokenizer
        String str = "abc,.def,hij,klm";
        StringTokenizer st = new StringTokenizer(str, ".,\n");
        //StringTokenizer st = new StringTokenizer(str, ".,\n", true);
        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }


        String str2 = "Hello World! I   like java   programming!";
        StringTokenizer st2 = new StringTokenizer(str2);
        //StringTokenizer st = new StringTokenizer(str, ".,\n", true);
        while (st2.hasMoreTokens()) {
            System.out.println(st2.nextToken());
        }

    }
}
