/*
 * The programs are designed for PDC paper
 */
package PDC.C_IO;

import java.io.*;

/**
 * @author Quan Bai and Weihua Li
 * @Note this is a recommended way to read and write text files
 */
public class C04_Buffered {

    public static void main(String args[]) {
        try {
            // Create buffered reader and writer.
            BufferedReader br = new BufferedReader(new FileReader("sample.txt"));
            BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
            // Create a variable to store a line
            String line = "";
            while ((line = br.readLine()) != null) {
                //process the text for each line
                line = line.toUpperCase();
                System.out.println(line);
                bw.append(line);
                bw.newLine();
            }
            br.close();
            bw.close();
        } catch (IOException ex) {
            System.err.println("IOException Error: " + ex.getMessage());
        }
    }
}
