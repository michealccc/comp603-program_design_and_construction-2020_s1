package com.student1079154.task1.textconvert;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author @author Micheal Chan <1079154> <skk8435@autuni.ac.nz>
 *
 */
public class TextConvert {

    public static void begin() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));

            String line;
            
            // Loop until end of text file
            while ((line = reader.readLine()) != null) {
                StringBuilder stringBuilder = new StringBuilder();

                // Input of file
                System.out.println("-----Input-----");
                System.out.println(line);

                // Convert all letters to uppercase
                line = line.toUpperCase();

                // Remove non-letters
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) <= 90 && line.charAt(i) >= 65) {
                        stringBuilder.append(line.charAt(i));
                    }
                }

                // Reverse the string
                stringBuilder.reverse();

                // Output of file
                System.out.println("-----Output-----");
                System.out.println(stringBuilder.toString());

                // Write the string line to output.txt
                writer.append(stringBuilder.toString());
                writer.newLine();

            }
            // close reader and writer
            reader.close();
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // start
        begin();
    }
}
