package com.student1079154.task2.studentscore;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Micheal Chan <1079154> <skk8435@autuni.ac.nz>
 *
 */
public class StudentScore {

    public static HashMap readFile() {
        HashMap map = new HashMap();

        try {
            BufferedReader reader = new BufferedReader(new FileReader("scores.txt"));
            String line;
            // Read a line from file
            while ((line = reader.readLine()) != null) {

                // Process line for name and mark
                StringTokenizer tokenizer = new StringTokenizer(line, " ");

                // Check for elements in line delimited by space should be two elements
                while (tokenizer.hasMoreElements()) {

                    //TODO: throw expection if there isn't two elements
                    if (tokenizer.countTokens() != 2) {
                        System.out.println("Incorrect format");
                    }
                    // Student name
                    String key = tokenizer.nextToken();
                    // Student mark
                    String value = tokenizer.nextToken();
                    // Add student name and mark into the map
                    map.put(key, value);
                }
            }

            reader.close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }

        return map;
    }

    public static void printMap(Map map) {
        StringBuilder builder = new StringBuilder();

        for (Object o : map.keySet()) {
            builder.append("Key: ").append(o.toString()).append(" | ");
            builder.append("Value: ").append(map.get(o)).append("\n");
        }

        System.out.println(builder.toString());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //TODO: The program loads the existing marks from scores.txt without overwriting the content in the file.
        HashMap studentMap = readFile();
        printMap(studentMap);

        //TODO:The program prompt the user to input a student name (first name only), and his/her exam mark.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter students name and mark. Format: \"John 18\"");
        System.out.print("> ");
        String studentNameInput = scanner.nextLine();

        //System.out.println("Student score:");
        //System.out.print("> ");
        //int studentScoreInput = scanner.nextInt();
        // Check for student
        // Check for mark
        
        //TODO:The program then check whether the mark of the student has been recorded already.
        String i = (String) studentMap.get(studentNameInput);

        if (i != null) {
            System.out.println("Student: " + studentNameInput + ", Mark: " + i);
            
            System.out.println("Do you want to overwrite "+studentNameInput+"'s mark?");
        } else {
            System.out.println(studentNameInput + " was not found.");
        }

        //printMap(studentMap);
        //TODO:If the record is existing already, the program checks whether the user wants to overwrite the record. If
        // so, the record will be overwritten; otherwise, prompt for the next input.
        
        
        
        //TODO:The program will quit after the user inputs x.
        //TODO:
    }

}
