package task1;

public class Counter implements Runnable {

    int num;

    public static void main(String[] args) {
        Counter count1 = new Counter(1);
        Thread thread1 = new Thread(count1);

        Counter count2 = new Counter(2);
        Thread thread2 = new Thread(count2);

        thread1.start();
        thread2.start();
    }

    public Counter(int i) {
        this.num = i;
    }

    public void printNum() {
        for (int j = this.num; j <= 10; j += 2) {
            System.out.print(j + " ");
        }
    }

    @Override
    public void run() {
        printNum();
        
    }

}
