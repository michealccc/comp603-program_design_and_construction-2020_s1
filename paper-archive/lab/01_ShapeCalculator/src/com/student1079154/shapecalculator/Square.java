/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student1079154.shapecalculator;

/**
 *
 * @author 1079154 Micheal Chan
 */
public class Square extends Rectangle {

    /**
     * @param length the length of one side of an square
     */
    public Square(double length) {
        super(length, length);
        super.setName("Square");
    }

    @Override
    public void printInfo() {
        System.out.println(this.shapeName);
        System.out.println(String.format("%.2f", this.area));
    }

}
