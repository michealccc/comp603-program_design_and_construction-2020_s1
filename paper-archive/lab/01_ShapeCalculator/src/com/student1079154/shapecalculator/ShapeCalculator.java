/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student1079154.shapecalculator;

import java.util.Scanner;

/**
 *
 * @author 1079154 Micheal Chan
 */
public class ShapeCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        begin();

    }

    public static void begin() {
        Scanner scan = new Scanner(System.in);

        int userInput;
        double userInputWidth, userInputLength, userInputRadius;

        System.out.println("Please select:");
        System.out.println("(1) Rectangle");
        System.out.println("(2) Circle");
        System.out.println("(3) Square");
        System.out.print("> ");

        userInput = scan.nextInt();

        System.out.println("-------------------------");

        switch (userInput) {
            case 1:
                System.out.println("Rectangle Width? ");
                System.out.print("> ");

                userInputWidth = scan.nextDouble();

                System.out.println("Rectangle Length? ");
                System.out.print("> ");

                userInputLength = scan.nextDouble();

                Rectangle recObj = new Rectangle(userInputWidth, userInputLength);

                System.out.println("-------------------------");
                recObj.printInfo();

                break;
            case 2:
                System.out.println("Radius of circle? ");
                System.out.print("> ");
                userInputRadius = scan.nextDouble();

                Circle cirObj = new Circle(userInputRadius);

                System.out.println("-------------------------");
                cirObj.printInfo();
                break;
            case 3:
                System.out.println("Length of square? ");
                System.out.print("> ");
                userInputLength = scan.nextDouble();

                Square squObj = new Square(userInputLength);

                System.out.println("-------------------------");
                squObj.printInfo();
                break;
            default:
                System.out.println("-------------------------");
                System.out.println("Invalid Input. Try again.");
                break;
        }
    }



}
