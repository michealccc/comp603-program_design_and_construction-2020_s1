/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student1079154.shapecalculator;

/**
 *
 * @author 1079154 Micheal Chan
 */
public class Rectangle extends Shape {

    private double width;
    private double length;

    /**
     * @param width the width of an rectangle
     * @param length the length of an rectangle
     */
    public Rectangle(double width, double length) {
        super("Rectangle");
        this.width = width;
        this.length = length;
        calculateArea();
    }

    @Override
    public void calculateArea() {
        // area = width * length
        this.area = this.width * this.length;
    }

}
