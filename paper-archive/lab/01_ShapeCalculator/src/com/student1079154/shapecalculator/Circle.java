/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.student1079154.shapecalculator;

/**
 *
 * @author 1079154 Micheal Chan
 */
public class Circle extends Shape {

    private double radius;

    /**
     * @param radius the radius of a circle
     */
    public Circle(double radius) {
        super("Circle");
        this.radius = radius;
        calculateArea();
    }

    @Override
    public void calculateArea() {
        // area = Pi * radius^2
        this.area = Math.PI * Math.pow(this.radius, 2);
    }

    @Override
    public void printInfo() {
        System.out.println(this.shapeName);
        System.out.println(String.format("%.3f", this.area));
    }

}
