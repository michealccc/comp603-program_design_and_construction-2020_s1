package mathquiz;

import java.text.NumberFormat;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

public class QuizMain {

    Random r = new Random();

    public static void main(String[] args) {
        QuizMain qm = new QuizMain();
        Scanner scanner = new Scanner(System.in);

        int score = 0;
        int rounds = 3;

//        Pattern pattern = Pattern.compile("123456789");
//        
//        System.out.println("===Math Quiz====");
//        
//        
//        while (rounds != 0) {
//            System.out.println("Rounds left: " + rounds);
//
//            
//            System.out.print("Question: ");
//            double cAnswer = qm.generateQuestion();
//            System.out.println("[DEBUG \"answer\": " + cAnswer+"]");
//
//            System.out.println("Your answer:");
//            System.out.print("> ");
//            
//            while (scanner.hasNext(pattern)) {                
//                System.out.println("Matches!");
//            }
//            uAnswer = scanner.nextDouble();
//
//            score += qm.checkAnswer(uAnswer, cAnswer);
//            
//            System.out.println("You got: " + score);
//            --rounds;
//            System.out.println("----------------");
//        }
        String userInput;
        boolean terminate = false;

        for (int round = 0; round <= rounds; round++) {
            System.out.println("Round" + (round + 1));
            
            double correctAnswer = qm.generateQuestion();
            double uAnswer = 0;
            boolean valid = false;
            
            do {
                userInput = scanner.nextLine();
                if (userInput.trim().equals("x")) {
                    terminate = true;
                    break;
                }
                try {
                    uAnswer = Double.parseDouble(userInput);
                    valid = true;
                } catch (NumberFormatException e) {
                    valid = false;
                    System.out.println("Incorrect Input. " + e.getMessage());
                }
            } while (!valid);
            
            if (terminate == true) {
                break;
            }
        }

    }

    public double generateQuestion() {

        int a = generateNumber(100);
        int b = generateNumber(100);
        char operator = 0;
        double answer = 0;

        //r.nextInt(4)
        switch (3) {
            case 0:
                operator = '+';
                break;
            case 1:
                operator = '-';
                break;
            case 2:
                operator = '*';
                break;
            case 3:
                while (b < a) {
                    b = generateNumber(100);
                }
                operator = '/';
                break;
        }

        System.out.println(a + " " + operator + " " + b);

        switch (operator) {
            case '+':
                answer = a + b;
                break;
            case '-':
                answer = a - b;
                break;
            case '*':
                answer = a * b;
                break;
            case '/':
                answer = a / b;
                break;
        }

        return answer;

    }

    public int generateNumber(int range) {
        return r.nextInt((range - 1) + 1) + 1;
    }

    public int checkAnswer(double uAnswer, double cAnswer) {
        int score;
        if (cAnswer == uAnswer) {
            System.out.println("Correct!");
            score = 10;
        } else {
            System.out.println("Incorrect!");
            score = -10;
        }

        //if(Math.abs(cAnswer - uAnswer) <= 0.1)
        return score;

    }

}
