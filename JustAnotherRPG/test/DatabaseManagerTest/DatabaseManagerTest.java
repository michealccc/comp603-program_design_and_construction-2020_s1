/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabaseManagerTest;

import com.gamedatabase.DatabaseManager;
import org.junit.Test;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

/**
 * This class tests that the database connection can be created and started successfully
 * @author 1079154 Micheal Chan
 */
public class DatabaseManagerTest {
    
    public DatabaseManagerTest() {
    }
    
    /**
     * Tests connection is successful by checking if there are any system tables in the database
     * in a ResultSet.
     * @throws SQLException 
     */
    @Test
    public void gameDatabaseConnectionTest() throws SQLException {
        DatabaseManager tdb = new DatabaseManager();
        DatabaseMetaData dbmd = tdb.getConnection().getMetaData();
        ResultSet rs = dbmd.getTables(null, null, "%", null);
        assertEquals(true,rs.next());
    }
}
