package ActorTest;

import com.justanotherrpg.actor.Actor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author 1079154 Micheal Chan
 */
public class ActorTest {
    
    public ActorTest() {
    }
    
    @Test
    public void newPlayerMaxHP() {
        Actor player = new Actor(0,0,"test");
        assertEquals(100,player.getMaxHP());
    }

    @Test
    public void newPlayerCurrentHP() {
        Actor player = new Actor(0,0,"test");
        assertEquals(100,player.getCurrentHP());
    }
}
