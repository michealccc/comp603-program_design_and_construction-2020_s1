package GameModelTest;

import com.justanotherrpg.actor.Actor;
import com.justanotherrpg.model.GameModel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author 1079154 Micheal Chan
 */
public class GameModelTest {
    
    public GameModelTest() {
    }
   
    @Test
    public void drinkPotionIncrementHealthTest() {
        Actor player = new Actor(0,0,"test");
        player.setCurrentHP(70);
        GameModel gameModel = new GameModel(player);
        gameModel.setNPotions(1);
        gameModel.drinkPotion();
        assertEquals(80,player.getCurrentHP());
    }
    
        @Test
    public void drinkPotionIncrementToMaxHealthTest() {
        Actor player = new Actor(0,0,"test");
        player.setCurrentHP(98);
        GameModel gameModel = new GameModel(player);
        gameModel.setNPotions(1);
        gameModel.drinkPotion();
        assertEquals(100,player.getCurrentHP());
    }

    @Test
    public void NumberOfPotionsAtStartOfGameTest() {
        Actor player = new Actor(0,0,"test");
        GameModel gameModel = new GameModel(player);
        assertEquals(0,gameModel.getNPotions());
    }
    
}
