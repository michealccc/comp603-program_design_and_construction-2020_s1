job_name:Fighter
job_type:JOB_WARRIOR
weapon_choice:Unarmed,Dagger,Long Sword,Hand Axe,Mace,Whip,Halberd
equipment: mail,shield,potion
strength: 8
intelligence: 0
dexterity: 4
skill_points: 2
hp: 0
mp: 0
exp: 0
fighting: 3
short_blades: 0
long_blades: 0
axe: 0
mace_flail: 0
polearm: 0
staff: 0
unarmed: 0
spellcasting: 0

job_name:Monk
job_type:JOB_WARRIOR
weapon_choice:Mace,Whip
equipment:Robe,Apple
strength: 3
intelligence: 2
dexterity: 7
skill_points: 3
hp: 0
mp: 0
exp: 0
fighting: 3
short_blades: 0
long_blades: 0
axe: 0
mace_flail: 0
polearm: 0
staff: 0
unarmed: 0
spellcasting: 0

job_name:Wizard
job_type:JOB_MAGE
weapon_choice:Staff
equipment:Robe,Hat,Book,Apple
strength: 3
intelligence: 2
dexterity: 7
skill_points: 3
hp: 0
mp: 0
exp: 0
fighting: 0
short_blades: 0
long_blades: 0
axe: 0
mace_flail: 0
polearm: 0
staff: 1
unarmed: 0
spellcasting: 3
