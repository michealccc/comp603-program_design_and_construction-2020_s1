package com.justanotherrpg.model;

import com.justanotherrpg.GUIGameApp;
import com.justanotherrpg.actor.Actor;
import com.justanotherrpg.view.CardLayoutView;
import com.justanotherrpg.view.RunGameView;

import java.util.Random;

/**
 * This class manages the game data and mechanisms, including game progress,
 * player health increase or decrease, storing and drinking potions and monsters.
 * Randomness is used to create each game so each time the game is played it will
 * not be the same as the previous game.
 *
 * @author 1079154 Micheal Chan
 */
public class GameModel {
    private static int progress;
    private static int camelFightBegins;
    private static int monkeyFightBegins;
    private static int kingOfTheJungleFightBegins;
    private final Actor player;
    private final Random rand;
    private RunGameView runGameView;
    private String message;
    private int nPotions;

    /**
     * The constructor starts the game with progress and number of potions as 0.
     * The constructor also sets the time at which the monsters will appear
     * (randomness is used to create variety so the game is not always the same).
     *
     * @param player the player
     */
    public GameModel(Actor player) {
        this.player = player;
        this.rand = new Random();
        nPotions = 0;
        progress = 0;
        camelFightBegins = 20 + rand.nextInt(10) + 1;
        monkeyFightBegins = 35 + rand.nextInt(10) + 1;
        kingOfTheJungleFightBegins = 50 + rand.nextInt(10) + 1;
    }

    public int getNPotions() {
        return nPotions;
    }

    public void setNPotions(int n) {
        this.nPotions = n;
    }

    /**
     * When a player drinks a potion, they heal 10 health points, but health can
     * never exceed the maximum HP.
     */
    public void drinkPotion() {
        nPotions--;
        player.setCurrentHP(Math.min(player.getCurrentHP() + 10, player.getMaxHP()));
    }

    /**
     * Updates the game for when the player decides to take a walk action.
     * Walk actions will progress the game and will cause monsters to appear sooner.
     * Random events can appear when walking which can cause the player to lose health.
     */
    public void walkAction() {
        if (progress == camelFightBegins) {
            fightCamels();
        } else if (progress == monkeyFightBegins) {
            fightMonkeys();
        } else if (progress == kingOfTheJungleFightBegins) {
            fightKingOfTheJungle();
        } else {
            int randInt = rand.nextInt(10) + 1;
            if (randInt < 5) {
                message = "You run into a stampede of one eyed one horned " +
                        "flying purple people eaters and lose " + randInt +
                        " health points";
                player.setCurrentHP(player.getCurrentHP() - randInt);
            } else {
                message = "You continue your journey through the jungle, " +
                        "squeezing yourself past a thicket of palm trees and " +
                        "vines";
            }
            runGameView = CardLayoutView.getRunGameView();
            runGameView.updateRunGameView();
            runGameView.getModel().add(0, message);
            progress += 2;
        }
    }

    /**
     * Updates the game for when the player decides to take a search action.
     * Search actions will progress the game and will cause monsters to appear sooner.
     * Random events can appear when walking which can cause the player to lose or gain health.
     */
    public void searchAction() {
        if (progress == camelFightBegins) {
            fightCamels();
        } else if (progress == monkeyFightBegins) {
            fightMonkeys();
        } else if (progress == kingOfTheJungleFightBegins) {
            fightKingOfTheJungle();
        } else {
            int randInt = rand.nextInt(10) + 1;
            if (randInt < 4) {
                message = "You get a snake caught in your boot while searching! " +
                        "You lose " + randInt + " health points";
                player.setCurrentHP(player.getCurrentHP() - randInt);
            } else if (randInt == 4) {
                message = "You find a potion! Better save this for a rainy day...";
                nPotions++;
                potionUpdate();
            } else if (randInt == 5 && player.getCurrentHP() != player.getMaxHP()) {
                message = "You find an edible mushroom with healing properties! " +
                        "You take a bite and gain 1 health point";
                player.setCurrentHP(player.getCurrentHP() + 1);
            } else {
                message = "You find a cute ladybug and spend a few moments " +
                        "playing with it before putting it back down onto a leaf";
            }
            runGameView = CardLayoutView.getRunGameView();
            runGameView.updateRunGameView();
            runGameView.getModel().add(0, message);
            progress++;
        }
    }

    /**
     * Updates the GUI when the player drinks or finds a potion - enabling/disabling
     * the button to drink potions as required and updating the number of potions available
     * in the display.
     */
    public void potionUpdate() {
        CardLayoutView.getRunGameView().
                getDrinkPotionButton().setText("Drink potion (" +
                GUIGameApp.getGameModel().getNPotions() + ")");
        CardLayoutView.getRunGameView().
                getDrinkPotionButton().setEnabled(nPotions != 0);
    }

    /**
     * Updates the game when the player decides to flee an enemy. The player
     * will always lose half their health when fleeing a monster.
     */
    public void fleeAction() {
        player.setCurrentHP(player.getCurrentHP() / 2);
        runGameView = CardLayoutView.getRunGameView();
        runGameView.updateRunGameView();
        runGameView.getModel().add(0, "You flee the scene...you " +
                "manage to but not without getting one final attack of " +
                "retaliation from the enemy!");
        runGameView.getModel().add(0, "You lose half your health" +
                " points");
        runGameView.getFightButton().setEnabled(false);
        runGameView.getFleeButton().setEnabled(false);
        runGameView.getWalkButton().setEnabled(true);
        runGameView.getSearchButton().setEnabled(true);
        progress++;
    }

    /**
     * This method starts the fight against monsters and updates the GUI to
     * start the fight mode which disables the player from walk/search actions
     * and enables fight/flee actions.
     */
    public void fightCamels() {
        runGameView.getFightButton().setEnabled(true);
        runGameView.getFleeButton().setEnabled(true);
        runGameView.getWalkButton().setEnabled(false);
        runGameView.getSearchButton().setEnabled(false);
        message = "Two camels suddenly appear in your tracks! They look angry" +
                "...";
        runGameView.getModel().add(0, message);
    }

    /**
     * This method starts the fight against monsters and updates the GUI to
     * start the fight mode which disables the player from walk/search actions
     * and enables fight/flee actions.
     */
    public void fightMonkeys() {
        runGameView.getFightButton().setVisible(true);
        runGameView.getFleeButton().setVisible(true);
        runGameView.getModel().add(0, "You hear a loud cry " +
                "from below and look down - looks like you've stepped on a" +
                "baby monkey's tail accidently");
        runGameView.getModel().add(0, "A larger monkey comes " +
                "scrambling towards you, scowling. Looks like a very angry " +
                "and protective parent...");
    }

    /**
     * This method starts the fight against monsters and updates the GUI to
     * start the fight mode which disables the player from walk/search actions
     * and enables fight/flee actions.
     */
    public void fightKingOfTheJungle() {
        runGameView.getFightButton().setVisible(true);
        runGameView.getFleeButton().setVisible(true);
        message = "Two camels suddenly appear in your tracks! They look angry" +
                "...";

        runGameView.getModel().add(0, message);
    }

}
