package com.justanotherrpg;

import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.loader.DataLoader;

/**
 * @author 1079154 Micheal Chan
 */
public class CLIGameApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Load existing game data
        DataLoader dataLoader = new DataLoader(new GameDatabase());
        GameController GameController = new GameController(dataLoader.getGameDatabase());

        // Load menu
        GameController.getMenu();
    }
}
