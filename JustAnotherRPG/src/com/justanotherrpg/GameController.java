package com.justanotherrpg;

import com.justanotherrpg.actor.Actor;
import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.Job;
import com.justanotherrpg.data.Species;
import com.justanotherrpg.world.Map;

import java.util.Scanner;

/**
 * Class that handles game creation
 *
 * @author 1079154 Micheal Chan
 */
public class GameController {
    private static final Scanner keyboard = new Scanner(System.in);
    private static final GameViewer viewer = new GameViewer();
    private final GameDatabase gameDatabase;
    private final int mapWidth = 7;
    private final int mapHeight = 3;
    //private int roomMaxSize = 10;
    //private int roomMinSize = 6;
    //private int numRoom = 30;

    public GameController(GameDatabase gameDatabase) {
        this.gameDatabase = gameDatabase;
    }

    /**
     * The game menu
     */
    public void getMenu() {
        String choice;

        // Start menu and wait for input
        while (true) {
            viewer.printMainMenu();
            choice = keyboard.nextLine();
            if (choice.contains("n")) {
                newGame();
                break;
            } else if (choice.contains("v")) {

                // Start database menu and wait for input
                boolean databaseMenu = true;
                viewer.printDatabaseMenu();

                while (databaseMenu) {
                    choice = keyboard.nextLine();

                    if (choice.contains("1")) { // Print weapon database
                        viewer.printDatabase(gameDatabase, 1);
                        viewer.printDatabaseMenu();
                    } else if (choice.contains("2")) { // Print jobs database
                        viewer.printDatabase(gameDatabase, 2);
                        viewer.printDatabaseMenu();
                    } else if (choice.contains("3")) { // Print species database
                        viewer.printDatabase(gameDatabase, 3);
                        viewer.printDatabaseMenu();
                    } else if (choice.contains("b")) {
                        databaseMenu = false;
                    }
                }
            } else if (choice.contains("q")) { // Exit program
                System.exit(0);
            }
        }

    }

    /**
     * Creates a new game
     */
    public void newGame() {
        // Create a new player
        Actor player = createNewCharacter();
        // Create a map to play in
        Map map = createMap(player);
        // Start the game
        start(map, player);
    }

    /**
     * Function to create a map to play in
     *
     * @param player player actor object to determine player starting location on the map
     * @return a populated map
     */
    public Map createMap(Actor player) {
        Map map = new Map(mapWidth, mapHeight);
        //world.makeMap(numRoom, roomMinSize, roomMaxSize,mapWidth,mapHeight,player);
        map.makeStarterMap(player);

        viewer.printGameMap(map);
        return map;
    }

    /**
     * Creates a new player character
     *
     * @return a Actor object representing the player
     */
    public Actor createNewCharacter() {
        System.out.println("Name: ");
        System.out.print("> ");
        String playerName = keyboard.nextLine();

        // Function to pick a species
        Species aSpecies = speciesPicker();
        // Function to pick a job
        Job aJob = jobPicker();

        // Create new Player Actor
        Actor player = new Actor(1, 1, "@");

        // Set some basic player information
        player.setStringName(playerName);
        player.setSpecies(aSpecies);
        player.setJob(aJob);
        player.setPlayer(true);

        // TODO: calculate players stats

        return player;
    }

    /**
     * Function to pick a species at character creation
     *
     * @return a species for the player
     */
    public Species speciesPicker() {
        Species aSpecies;
        // Get the list available species
        viewer.listAvailablePlayerSpecies(gameDatabase);

        String choice;

        // TODO: Change selection code to use species code to retrieve species
        // TODO: Change species hardcoded values

        while (true) {
            choice = keyboard.nextLine();

            // TODO: Loop the selection
            if (choice.contains("1")) {
                aSpecies = gameDatabase.getSpeciesDatabase().get(1);
                break;
            } else if (choice.contains("2")) {
                aSpecies = gameDatabase.getSpeciesDatabase().get(2);
                break;
            }
            System.out.println("Invalid input.\n");
            viewer.listAvailablePlayerSpecies(gameDatabase);
        }
        return aSpecies;
    }

    /**
     * Function to pick a job at character creation
     *
     * @return a job for the player
     */
    public Job jobPicker() {
        Job aJob;
        // Get the list available jobs
        viewer.listAvailablePlayerJobs(gameDatabase);

        // TODO: Change selection code to use job code to retrieve species
        // TODO: Change job hardcoded values

        String choice;
        while (true) {
            choice = keyboard.nextLine();

            // TODO: Loop the selection
            if (choice.contains("1")) {
                aJob = gameDatabase.getJobsDatabase().get(1);
                break;
            } else if (choice.contains("2")) {
                aJob = gameDatabase.getJobsDatabase().get(2);
                break;
            } else if (choice.contains("3")) {
                aJob = gameDatabase.getJobsDatabase().get(3);
                break;
            }
            System.out.println("Invalid input.\n");
            viewer.listAvailablePlayerSpecies(gameDatabase);
        }
        return aJob;
    }

    /**
     * Start the game
     *
     * @param map    map instance
     * @param player player instance
     */
    public void start(Map map, Actor player) {
        System.out.println("You wake up.");
        System.out.println("You are " + player.getStringName() + " a " + player.getSpecies().getSpeciesName() + " " + player.getJob().getJobName());
    }
}
