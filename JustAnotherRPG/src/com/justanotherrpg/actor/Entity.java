package com.justanotherrpg.actor;

/**
 * A object that represents players, enemies and items
 *
 * @author 1079154 Micheal Chan
 */
public class Entity {
    private int positionX;
    private int positionY;

    /**
     * Constructor to create a new entity type object
     *
     * @param positionX the entity's x position.
     * @param positionY the entity's y position.
     * @param glyph     a symbol to represent the entity.
     */
    public Entity(int positionX, int positionY, String glyph) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    /**
     * Method to move an entity to a location
     *
     * @param positionX the x position to move.
     * @param positionY the y position to move.
     */
    public void move(int positionX, int positionY) {
        this.positionX += positionX;
        this.positionY += positionY;
    }
}
