package com.justanotherrpg.actor;

import com.justanotherrpg.data.Job;
import com.justanotherrpg.data.Species;

/**
 * Class that represents a player and monsters.
 *
 * @author 1079154 Micheal Chan
 */
public class Actor extends Entity {
    private String stringName;
    private Species species;
    private Job job;

    private int level;
    private int maxHP;
    private int currentHP;
    private int experience;

    private int strength;
    private int intelligence;
    private int dexterity;

    private boolean isPlayer;

    /**
     * @param positionX x position of actor
     * @param positionY y position of actor
     * @param glyph     symbol representing the actor
     */
    public Actor(int positionX, int positionY, String glyph) {
        super(positionX, positionY, glyph);
        this.stringName = "stringName";
        this.species = null;
        this.job = null;
        this.isPlayer = false;
        this.currentHP = this.maxHP = 100;
    }

    /**
     * Gets the actor name
     *
     * @return a string of the actor name
     */
    public String getStringName() {
        return stringName;
    }

    /**
     * Sets the actor name
     *
     * @param stringName string of actor name
     */
    public void setStringName(String stringName) {
        this.stringName = stringName;
    }

    /**
     * Gets the actor species
     *
     * @return species of the actor
     */
    public Species getSpecies() {
        return species;
    }

    /**
     * Sets the actor species
     *
     * @param species a species
     */
    public void setSpecies(Species species) {
        this.species = species;
    }

    /**
     * Gets the actor job
     *
     * @return job of the actor
     */
    public Job getJob() {
        return job;
    }

    /**
     * Sets the actor job
     *
     * @param job a job
     */
    public void setJob(Job job) {
        this.job = job;
    }

    /**
     * Gets the actor level value
     *
     * @return level value of actor
     */
    public int getLevel() {
        return level;
    }

    /**
     * Sets the actor level value
     *
     * @param level value for level
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Gets the actor maximum hit points value
     *
     * @return maximum hit points value of actor
     */
    public int getMaxHP() {
        return maxHP;
    }

    /**
     * Sets the actor maximum hit points value
     *
     * @param maxHP value for maximum hit points
     */
    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    /**
     * Gets the actor current hit points value
     *
     * @return current hit points value of actor
     */
    public int getCurrentHP() {
        return currentHP;
    }

    /**
     * Sets the actor current hit points value
     *
     * @param currentHP value for current hit points
     */
    public void setCurrentHP(int currentHP) {
        this.currentHP = currentHP;
    }

    /**
     * Gets the actor experience point value
     *
     * @return experience point value of actor
     */
    public int getExperience() {
        return experience;
    }

    /**
     * Sets the actor experience value
     *
     * @param experience value for experience
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * Gets the actor strength value
     *
     * @return strength value of actor
     */
    public int getStrength() {
        return strength;
    }

    /**
     * Sets the actor strength value
     *
     * @param strength value for strength
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * Gets the actor intelligence value
     *
     * @return intelligence value of actor
     */
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Sets the actor intelligence value
     *
     * @param intelligence value of intelligence
     */
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * Gets the actor dexterity value
     *
     * @return dexterity value of actor
     */
    public int getDexterity() {
        return dexterity;
    }

    /**
     * Sets the actor dexterity value
     *
     * @param dexterity value of dexterity
     */
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    /**
     * Gets the boolean for a player.
     *
     * @return true: if the actor is a player | false: if the actor is not a player
     */
    public boolean isPlayer() {
        return isPlayer;
    }

    /**
     * Sets actors boolean value for a player
     *
     * @param player true: if the actor is a player | false: if the actor is not a player
     */
    public void setPlayer(boolean player) {
        isPlayer = player;
    }
}
