package com.justanotherrpg.actor;

/**
 * Class to retrieve information on actors
 *
 * @author 1079154 Micheal Chan
 */
public class ActorView {

    /**
     * Prints the player's details.
     *
     * @param playerName    the player's name
     * @param playerSpecies the player's species
     * @param playerJob     the player's job
     */
    public void printPlayerDetails(String playerName, String playerSpecies, String playerJob) {
        System.out.println("Player: " + playerName);
        System.out.println("Species: " + playerSpecies);
        System.out.println("Job: " + playerJob);
    }

    /**
     * Print the monster's details
     *
     * @param monsterName    the monster's name
     * @param monsterSpecies the monster's species
     * @param monsterJob     the monster's job
     */
    public void printMonsterDetails(String monsterName, String monsterSpecies, String monsterJob) {
        System.out.println("Player: " + monsterName);
        System.out.println("Species: " + monsterSpecies);
        System.out.println("Job: " + monsterJob);
    }

}
