package com.justanotherrpg;

import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.world.Map;
import com.justanotherrpg.world.Tile;

/**
 * Class to retrieve user views of information for the game
 *
 * @author 1079154 Micheal Chan
 */
public class GameViewer {
    public void printTitle() {
        System.out.println("|================================================================================|");
        System.out.println("|                     Welcome to JustAnotherRPG Simulator                        |");
        System.out.println("| -------------------------------------------------------------------------------|");
        System.out.println("|                                                                                |");
    }

    public void printPrompt() {
        System.out.println("|                                                                                |");
        System.out.println("|================================================================================|");
        System.out.print("> ");
    }

    public void printMainMenu() {
        printTitle();
        System.out.println("|                    n) New Game (Create new character)                          |");
        System.out.println("|                    v) View Game Database                                       |");
        System.out.println("|                    q) Quit                                                     |");
        printPrompt();
    }

    public void printDatabaseMenu() {
        printTitle();
        System.out.println("|                    1) Weapon database                                          |");
        System.out.println("|                    2) Job database                                             |");
        System.out.println("|                    3) Species database                                         |");
        System.out.println("|                    b) back                                                     |");
        printPrompt();

    }

    /**
     * Gets a list of available player character species
     *
     * @param gameDatabase the game database that contains the game data
     */
    public void listAvailablePlayerSpecies(GameDatabase gameDatabase) {
        System.out.println("Pick a species:");

        for (int i = 0; i < gameDatabase.getSpeciesDatabase().size(); i++) {
            // Print the first letter of species name
            System.out.print((i + 1) + ") ");
            // Print the species name
            System.out.println(gameDatabase.getSpeciesDatabase().get(i).getSpeciesName());
        }
    }

    /**
     * Gets a list of available player character jobs
     *
     * @param gameDatabase the game database that contains the game data
     */
    public void listAvailablePlayerJobs(GameDatabase gameDatabase) {
        System.out.println("Pick a job:");

        for (int i = 0; i < gameDatabase.getJobsDatabase().size(); i++) {
            // Print number selection
            System.out.print((i + 1) + ") ");
            // Print the species name
            System.out.println(gameDatabase.getJobsDatabase().get(i).getJobName());
        }
    }

    /**
     * Prints the map
     *
     * @param world a map object that contains populated map world
     */
    public void printGameMap(Map world) {
        Tile[][] map = world.getTiles();
        for (int i = 0; i < map.length + 2; i++) {
            System.out.print("-");
        }
        System.out.print("\n");

        for (int y = 0; y < world.getMapHeight(); y++) {
            System.out.print("|");
            for (int x = 0; x < world.getMapWidth(); x++) {
                // If a block is impassable

                if (map[x][y].isWall()) { // Print # as representation of a wall
                    System.out.print("#");
                } else if (!map[x][y].isWall()) { // Print . as representation as open space
                    System.out.print(".");
                }
            }
            System.out.print("|\n");
        }

        for (int i = 0; i < map.length + 2; i++) {
            System.out.print("-");
        }
        System.out.print("\n");
    }

    /**
     * Prints the data stored in the game database
     *
     * @param gameDatabase game database
     * @param input        value to select which database to view
     */
    public void printDatabase(GameDatabase gameDatabase, int input) {
        if (input == 1) {
            for (int i = 0; i < gameDatabase.getWeaponDatabase().size(); i++) {
                System.out.println(gameDatabase.getWeaponDatabase().get(i).toString());
            }
        } else if (input == 2) {
            for (int i = 0; i < gameDatabase.getJobsDatabase().size(); i++) {
                System.out.println(gameDatabase.getJobsDatabase().get(i).toString());
            }
        } else if (input == 3) {
            for (int i = 0; i < gameDatabase.getSpeciesDatabase().size(); i++) {
                System.out.println(gameDatabase.getSpeciesDatabase().get(i).toString());
            }
        } else {
            System.out.println("How did we get here?");
        }

    }
}
