package com.justanotherrpg;

import com.gamedatabase.DatabaseManager;
import com.justanotherrpg.actor.Actor;
import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.loader.DataLoader;
import com.justanotherrpg.model.GameModel;
import com.justanotherrpg.view.CardLayoutView;

import javax.swing.*;

/**
 * This is the main method that instantiates all the necessary objects, including
 * the main JFrame UI component, and executes the GUI application.
 *
 * @author 1079154 Micheal Chan
 */
public class GUIGameApp extends JFrame {
    private static CardLayoutView cardLayoutView;
    private static Actor player;
    private static GameModel gameModel;
    private static DatabaseManager databaseManager;
    // model objects
    private final GameDatabase gameDatabase;
    private final DataLoader dataLoader;

    public GUIGameApp(String title) {
        super(title);

        // Create the model and view
        player = new Actor(0, 0, "@");
        gameDatabase = new GameDatabase();
        dataLoader = new DataLoader(gameDatabase);
        gameModel = new GameModel(player);
        databaseManager = new DatabaseManager();
        databaseManager.playerStatsSetup();
        cardLayoutView = new CardLayoutView(gameDatabase);
        getContentPane().add(cardLayoutView);

        // Action listener to close frame when when quit button is pressed
        cardLayoutView.getMainMenuView().getQuit().addActionListener(
                e -> dispose());
    }

    public static DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public static CardLayoutView getCardLayoutView() {
        return cardLayoutView;
    }

    public static GameModel getGameModel() {
        return gameModel;
    }

    public static Actor getPlayer() {
        return player;
    }

    // Main method runs the GUI application
    public static void main(String[] args) {
        JFrame frame = new GUIGameApp("Just another RPG");
        frame.setSize(330, 230);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public GameDatabase getGameDatabase() {
        return gameDatabase;
    }
}
