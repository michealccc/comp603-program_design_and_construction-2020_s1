package com.justanotherrpg.view;

import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.Weapon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * This is the view for the weapons database in the GUI application. It is
 * built as a subclass of JPanel.
 */
public class WeaponsDatabaseView extends JPanel {
    private final JLabel weaponsInfo;
    private final JButton backToGameDatabase;

    public WeaponsDatabaseView(GameDatabase gdb) {
        setLayout(null);
        setPreferredSize(new Dimension(600, 450));

        backToGameDatabase = new JButton("Back to game database");
        backToGameDatabase.setSize(170, 50);
        backToGameDatabase.setLocation(210, 20);
        add(backToGameDatabase);

        ArrayList<Weapon> weaponArrayList = gdb.getWeaponDatabase();
        weaponsInfo = new JLabel("<html>" + weaponArrayList.toString());
        weaponsInfo.setLocation(20, 10);
        weaponsInfo.setSize(600, 450);
        add(weaponsInfo);


    }

    public JButton getBackToGameDatabase() {
        return backToGameDatabase;
    }
}
