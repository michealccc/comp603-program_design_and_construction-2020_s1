package com.justanotherrpg.view;

import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.Species;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * This is the view for the species database in the GUI application. It is
 * built as a subclass of JPanel.
 */
public class SpeciesDatabaseView extends JPanel {
    private final JLabel speciesInfo;
    private final JButton backToGameDatabase;

    public SpeciesDatabaseView(GameDatabase gdb) {
        setLayout(null);
        setPreferredSize(new Dimension(600, 450));

        backToGameDatabase = new JButton("Back to game database");
        backToGameDatabase.setSize(170, 50);
        backToGameDatabase.setLocation(210, 20);
        add(backToGameDatabase);

        ArrayList<Species> speciesArrayList = gdb.getSpeciesDatabase();
        speciesInfo = new JLabel("<html>" + speciesArrayList.toString());
        speciesInfo.setLocation(20, 10);
        speciesInfo.setSize(600, 450);
        add(speciesInfo);
    }

    public JButton getBackToGameDatabase() {
        return backToGameDatabase;
    }
}
