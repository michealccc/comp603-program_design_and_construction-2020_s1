package com.justanotherrpg.view;

import com.gamedatabase.DatabaseManager;
import com.justanotherrpg.GUIGameApp;
import com.justanotherrpg.actor.Actor;
import com.justanotherrpg.model.GameModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This is the view for the running game in the GUI application. It is
 * built as a subclass of JPanel.
 */
public class RunGameView extends JPanel {

    // UI components
    private final JLabel playerStats;
    private final JList<String> actionsList;
    private final JButton walkButton;
    private final JButton searchButton;
    private final JButton fightButton;
    private final JButton fleeButton;
    private final JButton drinkPotionButton;
    private final JButton quitToMainMenuButton;
    private final JScrollPane actionsScrollPane;
    private final JPanel buttonPanel;
    private final Actor player;
    private final DefaultListModel<String> actionsModel;
    private final GameModel gameModel;
    private final DatabaseManager databaseManager;

    public RunGameView() {
        setLayout(new BorderLayout());
        player = GUIGameApp.getPlayer();
        gameModel = GUIGameApp.getGameModel();
        databaseManager = GUIGameApp.getDatabaseManager();
        playerStats = new JLabel("Player name: " + player.getStringName() +
                " HP: " + player.getCurrentHP() + "/" + player.getMaxHP() +
                " Total wins: " + databaseManager.totalWins() +
                " Total losses: " + databaseManager.totalLosses());
        add(playerStats, BorderLayout.NORTH);

        buttonPanel = new JPanel();
        walkButton = new JButton("Walk");
        walkButton.addActionListener(new walkActionListener());
        searchButton = new JButton("Search");
        searchButton.addActionListener(new searchListener());
        drinkPotionButton =
                new JButton("Drink potion (" +
                        GUIGameApp.getGameModel().getNPotions() + ")");
        drinkPotionButton.setEnabled(false);
        drinkPotionButton.addActionListener(new DrinkPotionListener());
        fightButton = new JButton("Fight");
        fleeButton = new JButton("Flee");
        fleeButton.addActionListener(new fleeActionListener());
        quitToMainMenuButton = new JButton("Quit to main menu");
        buttonPanel.add(walkButton);
        buttonPanel.add(searchButton);
        buttonPanel.add(drinkPotionButton);
        buttonPanel.add(fightButton);
        buttonPanel.add(fleeButton);
        buttonPanel.add(quitToMainMenuButton);
        add(buttonPanel, BorderLayout.SOUTH);

        fightButton.setEnabled(false);
        fleeButton.setEnabled(false);

        actionsModel = new DefaultListModel<>();
        actionsList = new JList<>(actionsModel);

        actionsScrollPane = new JScrollPane(actionsList,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        JScrollBar verticalScrollBar = actionsScrollPane.getVerticalScrollBar();
        verticalScrollBar.setValue(verticalScrollBar.getMaximum());

        actionsModel.add(0, "You wake up");
        actionsModel.add(0, "Start adventure!");
        add(actionsScrollPane, BorderLayout.CENTER);
    }

    public JButton getWalkButton() {
        return walkButton;
    }

    public JButton getSearchButton() {
        return searchButton;
    }

    public JButton getFightButton() {
        return fightButton;
    }

    public JButton getFleeButton() {
        return fleeButton;
    }

    public JButton getDrinkPotionButton() {
        return drinkPotionButton;
    }

    public DefaultListModel<String> getModel() {
        return actionsModel;
    }

    public JButton getQuitToMainMenuButton() {
        return quitToMainMenuButton;
    }

    public void updateRunGameView() {
        playerStats.setText("Player name: " + player.getStringName() +
                " HP: " + player.getCurrentHP() + "/" + player.getMaxHP() +
                "                                                                                          Total wins: " + databaseManager.totalWins() +
                " Total losses: " + databaseManager.totalLosses());
    }

    public static class walkActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            GUIGameApp.getGameModel().walkAction();
        }
    }

    public static class searchListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            GUIGameApp.getGameModel().searchAction();
        }
    }

    public class DrinkPotionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            updateRunGameView();
            actionsModel.add(0, "Mm, tastes like aloe vera, " +
                    "how refreshing. I feel rejuvenated!");
            gameModel.drinkPotion();
            gameModel.potionUpdate();
        }
    }

    public class fleeActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            gameModel.fleeAction();
        }
    }

}
