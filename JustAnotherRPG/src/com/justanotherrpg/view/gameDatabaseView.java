package com.justanotherrpg.view;

import com.justanotherrpg.data.GameDatabase;

import javax.swing.*;
import java.awt.*;

/**
 * This class is a subset of JPanel and manages the UI for viewing the game
 * database so that the player can see the various weapons, species and jobs
 * available in the game.
 *
 * @author 1079154 Micheal Chan
 */
public class GameDatabaseView extends JPanel {
    private final JButton viewWeapons;
    private final JButton viewSpecies;
    private final JButton viewJobs;
    private final JButton back;

    /**
     * The constructor creates the UI for the game database viewer main menu.
     *
     * @param gdb A GameDatabase object that should be managed in the main method.
     */
    public GameDatabaseView(GameDatabase gdb) {
        setLayout(null);
        setPreferredSize(new Dimension(600, 450));

        viewWeapons = new JButton("View Weapons database");
        viewSpecies = new JButton("View Species database");
        viewJobs = new JButton("View jobs database");
        back = new JButton("Back");

        viewWeapons.setLocation(170, 70);
        viewWeapons.setSize(240, 50);

        viewSpecies.setLocation(170, 130);
        viewSpecies.setSize(240, 50);

        viewJobs.setLocation(170, 190);
        viewJobs.setSize(240, 50);

        back.setLocation(170, 250);
        back.setSize(240, 50);

        add(viewWeapons);
        add(viewSpecies);
        add(viewJobs);
        add(back);
    }

    public JButton getViewWeapons() {
        return viewWeapons;
    }

    public JButton getViewSpecies() {
        return viewSpecies;
    }

    public JButton getViewJobs() {
        return viewJobs;
    }

    public JButton getBack() {
        return back;
    }
}