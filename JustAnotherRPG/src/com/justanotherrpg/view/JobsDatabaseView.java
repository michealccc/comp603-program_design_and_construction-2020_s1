package com.justanotherrpg.view;

import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.Job;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * This is the view for the jobs database in the GUI application. It is built
 * as a subclass of JPanel.
 */
public class JobsDatabaseView extends JPanel {
    private final JLabel jobsInfo;
    private final JButton backToGameDatabase;

    public JobsDatabaseView(GameDatabase gdb) {
        setLayout(null);
        setPreferredSize(new Dimension(300, 200));

        backToGameDatabase = new JButton("Back to game database");
        backToGameDatabase.setSize(170, 50);
        backToGameDatabase.setLocation(210, 20);
        add(backToGameDatabase);

        ArrayList<Job> jobsArrayList = gdb.getJobsDatabase();
        jobsInfo = new JLabel("<html>" + jobsArrayList.toString());
        jobsInfo.setLocation(20, 10);
        jobsInfo.setSize(600, 450);
        add(jobsInfo);


    }

    public JButton getBackToGameDatabase() {
        return backToGameDatabase;
    }
}
