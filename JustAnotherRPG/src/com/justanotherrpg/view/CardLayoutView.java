package com.justanotherrpg.view;

import com.justanotherrpg.GUIGameApp;
import com.justanotherrpg.data.GameDatabase;

import javax.swing.*;
import java.awt.*;

/**
 * This class manages all multiple panels and the Action Listeners for the GUI
 * application.
 */
public class CardLayoutView extends JPanel {
    private static JPanel mainPanel;
    private static MainMenuView mainMenuPanel;
    private static NewGameSetupView newGameSetupView;
    private static RunGameView runGameView;
    private static GameDatabaseView viewDatabasePanel;
    private static WeaponsDatabaseView weaponsDatabaseView;
    private static JobsDatabaseView jobsDatabaseView;
    private static SpeciesDatabaseView speciesDatabaseView;
    private static JOptionPane confirmQuitGame;
    private static CardLayout cl;


    /**
     * The panels for the multiple views of this app are created and added to this
     * high-level CardLayoutView panel. The action listeners for buttons and other
     * UI components are created and defined here.
     *
     * @param gdb GameDatabase object which should be created and managed in the main method
     */
    public CardLayoutView(GameDatabase gdb) {
        mainPanel = new JPanel();
        cl = new CardLayout();
        mainPanel.setLayout(cl);
        mainMenuPanel = new MainMenuView();
        newGameSetupView = new NewGameSetupView(gdb);
        runGameView = new RunGameView();
        viewDatabasePanel = new GameDatabaseView(gdb);
        weaponsDatabaseView = new WeaponsDatabaseView(gdb);
        jobsDatabaseView = new JobsDatabaseView(gdb);
        speciesDatabaseView = new SpeciesDatabaseView(gdb);

        mainPanel.add(mainMenuPanel, "Main Menu");
        mainPanel.add(newGameSetupView, "New Game Setup");
        mainPanel.add(runGameView, "Run Game");
        mainPanel.add(viewDatabasePanel, "View Game Database");
        mainPanel.add(weaponsDatabaseView, "Weapons Database");
        mainPanel.add(jobsDatabaseView, "Jobs Database");
        mainPanel.add(speciesDatabaseView, "Species Database");

        cl.show(mainPanel, "Main Menu");

        mainMenuPanel.getViewGameDatabase().addActionListener(e -> cl.show(mainPanel, "View Game Database"));

        viewDatabasePanel.getViewWeapons().addActionListener(e -> cl.show(mainPanel, "Weapons Database"));

        viewDatabasePanel.getViewJobs().addActionListener(e -> cl.show(mainPanel, "Jobs Database"));

        viewDatabasePanel.getViewSpecies().addActionListener(e -> cl.show(mainPanel, "Species Database"));

        viewDatabasePanel.getBack().addActionListener(e -> cl.show(mainPanel, "Main Menu"));

        weaponsDatabaseView.getBackToGameDatabase().addActionListener(e -> cl.show(mainPanel, "View Game Database"));

        jobsDatabaseView.getBackToGameDatabase().addActionListener(e -> cl.show(mainPanel, "View Game Database"));

        speciesDatabaseView.getBackToGameDatabase().addActionListener(e -> cl.show(mainPanel, "View Game Database"));

        mainMenuPanel.getNewGame().addActionListener(e -> {
            newGameSetupView.updateNewGameSetup();
            cl.show(mainPanel, "New Game Setup");
        });

        newGameSetupView.getStartGame().addActionListener(e -> {
            if (!newGameSetupView.getInputName()
                    .getText().trim().equals("") &&
                    (newGameSetupView.getJobsCheckboxes()[0].getState() ||
                            newGameSetupView.getJobsCheckboxes()[1].getState() ||
                            newGameSetupView.getJobsCheckboxes()[2].getState()) &&
                    newGameSetupView.getSpeciesCheckboxes()[0].getState() ||
                    newGameSetupView.getSpeciesCheckboxes()[0].getState()) {
                GUIGameApp.getPlayer().setStringName
                        (newGameSetupView.getInputName().getText());
                runGameView.updateRunGameView();

                cl.show(mainPanel, "Run Game");
            }
        });

        newGameSetupView.getBackToMainMenu().addActionListener(e -> cl.show(mainPanel, "Main Menu"));

        runGameView.getQuitToMainMenuButton().addActionListener(e -> {
            confirmQuitGame = new JOptionPane();
            int confirmQuitButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(runGameView,
                    "Are you sure you want to quit this game and " +
                            "return to the main menu? This game will count"
                            + "as a loss for the Player Stats.", "Quit game?",
                    confirmQuitButton);
            if (dialogResult == 0) {
                GUIGameApp.getDatabaseManager().insertGameLossRecord();
                cl.show(mainPanel, "Main Menu");
            }
        });

        add(mainPanel);
    }

    public static RunGameView getRunGameView() {
        return runGameView;
    }

    public NewGameSetupView getNewGameSetupView() {
        return newGameSetupView;
    }

    public MainMenuView getMainMenuView() {
        return mainMenuPanel;
    }
}
