package com.justanotherrpg.view;

import com.justanotherrpg.data.GameDatabase;

import javax.swing.*;
import java.awt.*;

/**
 * This is the view for the new game setup in the GUI application. It is
 * built as a subclass of JPanel.
 */
public class NewGameSetupView extends JPanel {
    private final GameDatabase gdb;
    // UI Components
    private final JTextArea inputName;
    private final JLabel nameLabel;
    private final JLabel species;
    private final JLabel job;
    private final JButton startGame;
    private final JButton backToMainMenu;
    private final CheckboxGroup speciesCheckBoxGroup;
    private final CheckboxGroup jobsCheckBoxGroup;
    private final Checkbox[] speciesCheckboxes;
    private final Checkbox[] jobsCheckboxes;

    public NewGameSetupView(GameDatabase gdb) {
        this.gdb = gdb;

        setLayout(new GridLayout(11, 1));
        setPreferredSize(new Dimension(600, 450));

        nameLabel = new JLabel("Please enter the player's name: ");
        inputName = new JTextArea();
        inputName.setSize(100, 30);

        species = new JLabel("Pick a species");

        speciesCheckBoxGroup = new CheckboxGroup();
        this.speciesCheckboxes = new Checkbox[gdb.getSpeciesDatabase().size()];

        for (int i = 0; i < gdb.getSpeciesDatabase().size(); i++) {
            String speciesName =
                    gdb.getSpeciesDatabase().get(i).getSpeciesName();

            speciesCheckboxes[i] = new Checkbox(speciesName, speciesCheckBoxGroup, false);
        }


        job = new JLabel("Pick a job");

        jobsCheckBoxGroup = new CheckboxGroup();
        this.jobsCheckboxes = new Checkbox[gdb.getJobsDatabase().size()];
        for (int i = 0; i < gdb.getJobsDatabase().size(); i++) {
            String jobName = gdb.getJobsDatabase().get(i).getJobName();
            jobsCheckboxes[i] = new Checkbox(jobName, jobsCheckBoxGroup, false);
        }

        startGame = new JButton("Start game");

        backToMainMenu = new JButton("Back to game database");

        add(nameLabel);
        add(inputName);
        add(species);
        for (Checkbox c : speciesCheckboxes) {
            add(c);
        }

        add(job);
        for (Checkbox c : jobsCheckboxes) {
            add(c);
        }
        add(startGame);
        add(backToMainMenu);
    }

    // Encapsulation - provide access to view components
    public JTextArea getInputName() {
        return inputName;
    }

    public JButton getStartGame() {
        return startGame;
    }

    public JButton getBackToMainMenu() {
        return backToMainMenu;
    }

    public Checkbox[] getSpeciesCheckboxes() {
        return speciesCheckboxes;
    }

    public Checkbox[] getJobsCheckboxes() {
        return jobsCheckboxes;
    }

    /**
     * Removes entered data from previous games so that the GUI is empty when you start a new game
     */
    public void updateNewGameSetup() {
        inputName.setText("");
        this.jobsCheckBoxGroup.setSelectedCheckbox(null);
        this.speciesCheckBoxGroup.setSelectedCheckbox(null);

    }
}
