package com.justanotherrpg.view;

import javax.swing.*;
import java.awt.*;

/**
 * This is the view for the main menu in the GUI application. It is built as a
 * subclass of JPanel.
 */
public class MainMenuView extends JPanel {
    // UI components
    private final JButton newGame;
    private final JButton viewGameDatabase;
    private final JButton quit;

    public MainMenuView() {

        setLayout(null);
        setPreferredSize(new Dimension(630, 450));

        newGame = new JButton("New Game");
        newGame.setLocation(170, 70);
        newGame.setSize(170, 50);
        add(newGame);

        viewGameDatabase = new JButton("View Game Database");
        viewGameDatabase.setLocation(170, 130);
        viewGameDatabase.setSize(170, 50);
        add(viewGameDatabase);

        quit = new JButton("Quit");
        quit.setLocation(170, 190);
        quit.setSize(170, 50);
        add(quit);
    }

    // Encapsulation - provide access to view components
    public JButton getNewGame() {
        return newGame;
    }

    public JButton getViewGameDatabase() {
        return viewGameDatabase;
    }

    public JButton getQuit() {
        return quit;
    }

}
