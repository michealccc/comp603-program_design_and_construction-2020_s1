package com.justanotherrpg.world;

/**
 * Representation of a tile.
 *
 * @author 1079154 Micheal Chan
 */
public class Tile {
    private boolean wall;
    private boolean fogOfWar;

    /**
     * @param wall true: if tile is a wall. | false: if tile is not a wall.
     */
    public Tile(boolean wall) {
        this.wall = wall;

        // Automatically make wall have fog of war.
        if (wall) {
            fogOfWar = true;
        }
    }

    /**
     * Gets boolean of wall.
     *
     * @return true: if tile is a wall. | false: if tile is not a wall.
     */
    public boolean isWall() {
        return wall;
    }

    /**
     * Sets boolean of wall.
     *
     * @param wall true: if tile is a wall. | false: if tile is not a wall.
     */
    public void setWall(boolean wall) {
        this.wall = wall;
    }

    /**
     * Sets boolean of sight.
     *
     * @param fogOfWar true: if tile can be seen. | false:  if tile can not be seen.
     */
    public void setFogOfWar(boolean fogOfWar) {
        this.fogOfWar = fogOfWar;
    }
}
