package com.justanotherrpg.world;

import com.justanotherrpg.actor.Actor;

/**
 * Representation of a map
 *
 * @author 1079154 Micheal Chan
 */
public class Map {
    private int mapWidth;
    private int mapHeight;
    private Tile[][] tiles;

    /**
     * Constructor to create map size
     *
     * @param mapWidth  width of playable map.
     * @param mapHeight height of playable map.
     */
    public Map(int mapWidth, int mapHeight) {
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        this.tiles = new Tile[mapWidth][mapHeight];
        initialiseTiles();
    }

    /**
     * Initialise all map tiles to walls.
     */
    public void initialiseTiles() {
        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                this.tiles[x][y] = new Tile(true);
            }
        }
    }

//    /**
//     * Generates a procedural map
//     *
//     * @param numRoom     - number of open spaces to create
//     * @param roomMinSize - minimum size a open space can be.
//     * @param roomMaxSize - maximum size a open space can be.
//     */
//    public void makeProceduralMap(int numRoom, int roomMinSize, int roomMaxSize) {
//        Random random = new Random();
//        Rect[] rooms = new Rect[numRoom];

//        for (int r = 0; r < numRoom; r++) {

//            // Generate some random open space sizes
//            int w = random.ints(roomMinSize, (roomMaxSize + 1)).limit(1).findFirst().getAsInt();
//            int h = random.ints(roomMinSize, (roomMaxSize + 1)).limit(1).findFirst().getAsInt();

//            // Generate some random positions
//            int x = random.ints(0, (this.mapWidth - w - 1)).limit(1).findFirst().getAsInt();
//            int y = random.ints(0, (this.mapHeight - h - 1)).limit(1).findFirst().getAsInt();

//            for (int i = 0; i < numRoom; i++) {

//            }
//        }
//    }

    public void makeStarterMap(Actor player) {
        createHorizontalTunnel(1, 5, 1);

        // TODO: Place player on map
    }

//    /**
//     * Method to create open spaces in the map.
//     *
//     * @param room a Rectable object that contains a size of a rectangle
//     */
//    public void createOpenSpace(Rectangle room) {
//        for (int x = room.getPositionX1() + 1; x < room.getPositionX2(); x++) {
//            for (int y = room.getPositionY1() + 1; y < room.getPositionY2(); y++) {
//                this.tiles[x][y].setWall(false);
//                this.tiles[x][y].setFogOfWar(false);
//            }
//        }
//    }

    /**
     * Method to create a horizontal tunnel on the map
     *
     * @param positionX1 start position of tunnel on x axis
     * @param positionX2 end position of tunnel determines length of tunnel x axis
     * @param positionY  position of tunnel on the y axis
     */
    public void createHorizontalTunnel(int positionX1, int positionX2, int positionY) {
        for (int x = Math.min(positionX1, positionX2); x < Math.max(positionX1, positionX2) + 1; x++) {
            this.tiles[x][positionY].setWall(false);
            this.tiles[x][positionY].setFogOfWar(false);
        }
    }

    /**
     * Method to create a vertical tunnel on the map
     *
     * @param positionY1 start position of tunnel on y axis
     * @param positionY2 end position of tunnel determines length of tunnel on y axis
     * @param positionX  position of tunnel on the x axis
     */
    public void createVerticalTunnel(int positionY1, int positionY2, int positionX) {
        for (int y = Math.min(positionY1, positionY2); y < Math.max(positionY1, positionY2) + 1; y++) {
            this.tiles[positionX][y].setWall(false);
            this.tiles[positionX][y].setFogOfWar(false);
        }
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public void setMapHeight(int mapHeight) {
        this.mapHeight = mapHeight;
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    public void setTiles(Tile[][] tiles) {
        this.tiles = tiles;
    }
}
