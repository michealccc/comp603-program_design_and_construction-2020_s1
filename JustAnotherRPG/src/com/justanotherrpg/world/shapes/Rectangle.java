package com.justanotherrpg.world.shapes;

/**
 * Representation of a rectangle.
 *
 * @author 1079154 Micheal Chan
 */
public class Rectangle {
    private final int positionX1;
    private final int positionY1;
    private final int positionX2;
    private final int positionY2;

    /**
     * @param positionX Position of rectangle on the x axis.
     * @param positionY Position of rectangle on the y axis.
     * @param width     Width length of rectangle.
     * @param height    Height length of rectangle.
     */
    public Rectangle(int positionX, int positionY, int width, int height) {
        this.positionX1 = positionX;
        this.positionY1 = positionY;
        this.positionX2 = positionX + width;
        this.positionY2 = positionY + height;
    }

    /**
     * Method to compare two squares intersecting.
     *
     * @param aRectangle a rectangle to compare.
     * @return true - if the rectangles intersect | false - if the rectangles do not intersect.
     */
    public boolean isIntersect(Rectangle aRectangle) {
        return this.positionX1 <= aRectangle.positionX2 && this.positionX2 >= aRectangle.positionX1 &&
                this.positionY1 <= aRectangle.positionY2 && this.positionY2 >= aRectangle.positionY1;
    }

    /**
     * Gets the value of starting rectangle x position.
     *
     * @return the value of positionX1
     */
    public int getPositionX1() {
        return positionX1;
    }

    /**
     * Gets the value of starting rectangle y position.
     *
     * @return the value of positionY1
     */
    public int getPositionY1() {
        return positionY1;
    }

    /**
     * Gets the value of rectangle x2 position.
     *
     * @return the value of positionX2
     */
    public int getPositionX2() {
        return positionX2;
    }

    /**
     * Gets the value of rectangle y2 position.
     *
     * @return the value of positionY2
     */
    public int getPositionY2() {
        return positionY2;
    }

    /**
     * @return a string representation of a Rectangle.
     */
    @Override
    public String toString() {
        return "Rectangle{" +
                "positionX1=" + positionX1 +
                ", positionY1=" + positionY1 +
                ", positionX2=" + positionX2 +
                ", positionY2=" + positionY2 +
                '}';
    }
}
