package com.justanotherrpg.data;

import com.justanotherrpg.data.types.JobType;

import java.util.Arrays;

/**
 * Representation of a job
 *
 * @author 1079154 Micheal Chan
 */
public class Job extends Modifier {
    private int jobCode;
    private JobType jobType;
    private String jobName;

    // Starting Equipment
    private String[] weaponChoice;
    private String[] equipment;

    private double skillPoints;

    public Job(int jobCode, JobType jobType, String jobName, double strength, double intelligence,
               double dexterity, double hp, double mp, double exp, double fighting, double short_blades,
               double long_blades, double axe, double mace_flail, double polearm, double staff, double unarmed,
               double spellcasting, String[] weaponChoice, String[] equipment, double skillPoints) {
        super(strength, intelligence, dexterity, hp, mp, exp, fighting, short_blades, long_blades, axe, mace_flail,
                polearm, staff, unarmed, spellcasting);
        this.jobCode = jobCode;
        this.jobType = jobType;
        this.jobName = jobName;
        this.weaponChoice = weaponChoice;
        this.equipment = equipment;
        this.skillPoints = skillPoints;
    }

    /**
     * Gets the job code
     *
     * @return job code
     */
    public int getJobCode() {
        return jobCode;
    }

    /**
     * Sets the job code
     *
     * @param jobCode job code
     */
    public void setJobCode(int jobCode) {
        this.jobCode = jobCode;
    }

    /**
     * Gets the job type
     *
     * @return job type
     */
    public JobType getJobType() {
        return jobType;
    }

    /**
     * Sets the job type
     *
     * @param jobType job type
     */
    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    /**
     * Gets the job name
     *
     * @return string value of job name
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Sets the job name
     *
     * @param jobName job name
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * Gets string array of weapon choice
     *
     * @return string array of weapon choice
     */
    public String[] getWeaponChoice() {
        return weaponChoice;
    }

    /**
     * Sets the string array weapon choice
     *
     * @param weaponChoice string array of weapon choice
     */
    public void setWeaponChoice(String[] weaponChoice) {
        this.weaponChoice = weaponChoice;
    }

    /**
     * Gets the string array of starting equipment
     *
     * @return starting equipment
     */
    public String[] getEquipment() {
        return equipment;
    }

    /**
     * Sets the string array of starting equipment
     *
     * @param equipment string array of starting equipment
     */
    public void setEquipment(String[] equipment) {
        this.equipment = equipment;
    }

    /**
     * Gets the skill points
     *
     * @return skill points
     */
    public double getSkillPoints() {
        return skillPoints;
    }

    /**
     * Sets the skill points
     *
     * @param skillPoints skill points
     */
    public void setSkillPoints(double skillPoints) {
        this.skillPoints = skillPoints;
    }

    /**
     * @return string representation of a job
     */
    @Override
    public String toString() {
        return "Job{" +
                "jobCode=" + jobCode +
                ", jobType=" + jobType +
                ", jobName='" + jobName + '\'' +
                ", weaponChoice=" + Arrays.toString(weaponChoice) +
                ", equipment=" + Arrays.toString(equipment) +
                ", skillPoints=" + skillPoints +
                '}';
    }
}
