package com.justanotherrpg.data;

import java.util.ArrayList;

/**
 * A representation of the game data, weapons, items, jobs.
 *
 * @author 1079154 Micheal Chan
 */
public class GameDatabase {
    private final ArrayList<Weapon> weaponDatabase;
    private final ArrayList<Job> jobsDatabase;
    private final ArrayList<Species> speciesDatabase;

    public GameDatabase() {
        this.weaponDatabase = new ArrayList<>();
        this.jobsDatabase = new ArrayList<>();
        this.speciesDatabase = new ArrayList<>();
    }

    /**
     * Adds a weapon to the weapon database
     *
     * @param weapon a weapon
     */
    public void putWeapon(Weapon weapon) {
        weaponDatabase.add(weapon);
    }

    /**
     * Gets a weapon from the database based on the weapon id
     *
     * @param code the weapon code
     * @return a weapon
     */
    public Weapon getWeaponCode(int code) {
        Weapon aWeapon = null;

        for (Weapon weapon : weaponDatabase) {
            if (code == weapon.getWeaponCode()) {
                aWeapon = weapon;
            }
        }
        return aWeapon;
    }

    /**
     * Gets the ArrayList Weapon
     *
     * @return the ArrayList Weapon
     */
    public ArrayList<Weapon> getWeaponDatabase() {
        return weaponDatabase;
    }

    /**
     * Adds a job to the job database
     *
     * @param job - a job
     */
    public void putJob(Job job) {
        jobsDatabase.add(job);
    }

    /**
     * Gets a job from the database based on the job id
     *
     * @param code the job code
     * @return a job
     */
    public Job getJobCode(int code) {
        Job aJob = null;

        for (Job job : jobsDatabase) {
            if (code == job.getJobCode()) {
                aJob = job;
            }
        }
        return aJob;
    }

    /**
     * Gets the ArrayList Job
     *
     * @return the ArrayList of Job
     */
    public ArrayList<Job> getJobsDatabase() {
        return jobsDatabase;
    }

    /**
     * Adds a species to the species database
     *
     * @param species a species
     */
    public void putSpecies(Species species) {
        speciesDatabase.add(species);
    }

    /**
     * Gets a species from the database based on the species id
     *
     * @param code the species code.
     * @return a species
     */
    public Species getSpeciesCode(int code) {
        Species aSpecies = null;

        for (Species species : speciesDatabase) {
            if (code == species.getSpeciesCode()) {
                aSpecies = species;
            }
        }
        return aSpecies;
    }

    /**
     * Gets the ArrayList for Species
     *
     * @return the ArrayList of Species
     */
    public ArrayList<Species> getSpeciesDatabase() {
        return speciesDatabase;
    }

    /**
     * @return string representation of GameDatabase.
     */
    @Override
    public String toString() {
        return "GameDatabase{" +
                "weaponDatabase=" + weaponDatabase +
                ", jobsDatabase=" + jobsDatabase +
                ", speciesDatabase=" + speciesDatabase +
                '}';
    }
}
