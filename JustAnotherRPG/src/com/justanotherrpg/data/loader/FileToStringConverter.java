package com.justanotherrpg.data.loader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Parses text files for the game, stored in the //game_data folder
 * and normalises and returns the data into an Array structure so that
 * it can be easily parsed via the DataLoader to create Job, Species and
 * Weapon objects.
 *
 * @author 1079154 Micheal Chan
 */
public class FileToStringConverter {
    public final String filePath;
    public String readFileString;

    /**
     * @param filePath filepath to the game text data
     */
    public FileToStringConverter(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Function that takes a text file and turns into a String ArrayList
     * Each String array represents attributes for one object.
     * Inside each String array consists of the object attributes as provided by the txt file.
     *
     * @param nAttributes number of attributes in a text file to parse per item.
     * @return attributes formatted into an ArrayList
     */
    public ArrayList<String[]> FileToString(int nAttributes) {

        //Creates new ArrayList to store all attributes for all attributes as defined in a text file.
        ArrayList<String[]> objectAttributes = new ArrayList<>();

        this.readFileString = "";
        try {
            byte[] readFileByte = Files.readAllBytes(Paths.get(this.filePath));
            //Converts the file into a String and cleans the escape character \r
            this.readFileString = new String(readFileByte).replace("\r", "");
        } catch (IOException e) {
            System.err.println("IO Exception, check local file path: " + e);
            return objectAttributes;
        }

        //Splits the complete file string by line, turning it into a String array.
        String[] fileStringArray = this.readFileString.split("\n");

        //Creating new string to represent attributes for 1 object
        String[] attributes = new String[nAttributes];

        //Track attribute index for parsing into the attributes String array.
        int attributeIndex = 0;

        for (String line : fileStringArray) {

            //If line contains data, store it in the attributes String array
            if (!line.equals("")) {
                //Only extract the data to the right of the colon e.g. strength:10, only "10" will be extracted
                String attribute = line.split(":")[1];
                attributes[attributeIndex] = attribute;
                attributeIndex++;
            } else {
                //If the line is empty this represents a space in the text file which means
                //that it is the end of the attribute data for that object, so a new attributes String array will be started.
                objectAttributes.add(attributes);
                attributes = new String[nAttributes];
                attributeIndex = 0;
            }
        }
        //Make sure the last objects attributes are added to the total object attributes list
        objectAttributes.add(attributes);

        return objectAttributes;
    }
}
