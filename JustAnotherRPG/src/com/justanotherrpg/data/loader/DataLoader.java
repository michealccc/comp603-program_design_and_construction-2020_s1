package com.justanotherrpg.data.loader;

import com.justanotherrpg.data.GameDatabase;
import com.justanotherrpg.data.Job;
import com.justanotherrpg.data.Species;
import com.justanotherrpg.data.Weapon;
import com.justanotherrpg.data.types.DamageType;
import com.justanotherrpg.data.types.JobType;
import com.justanotherrpg.data.types.SpeciesType;
import com.justanotherrpg.data.types.WeaponType;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Class to handle reading game data files.
 *
 * @author 1079154 Micheal Chan
 */
public class DataLoader {
    private final GameDatabase gameDatabase;

    public DataLoader(GameDatabase gameDatabase) {
        this.gameDatabase = gameDatabase;
        loadWeaponTextData();
        loadJobsTextData();
        loadSpeciesTextData();
    }

    /**
     * Loads weapons data from text file and creates Weapon objects
     */
    public void loadWeaponTextData() {
        // Location of weapon txt file stored
        String dataLocation = "game_data\\melee_weapon.txt";

        // Create a new FileToStringConverter object
        FileToStringConverter fileToStringConverter = new FileToStringConverter(dataLocation);
        //Parse the text file into an ArrayList of String Arrays. Each String array consists of the object attributes.
        ArrayList<String[]> objectAttributes = fileToStringConverter.FileToString(7);

        int code = 1;
        for (String[] speciesAttr : objectAttributes) {
            Weapon weapon = new Weapon(code,
                    speciesAttr[0],
                    WeaponType.valueOf(speciesAttr[1].toUpperCase()),
                    DamageType.valueOf(speciesAttr[2].toUpperCase()),
                    Double.parseDouble(speciesAttr[3]),
                    Double.parseDouble(speciesAttr[4]),
                    Double.parseDouble(speciesAttr[5]),
                    Boolean.parseBoolean(speciesAttr[6]));
            gameDatabase.putWeapon(weapon);
            code++;
        }
    }

    /**
     * Loads weapons data from text file and creates Weapon objects
     */
    public void loadJobsTextData() {
        // Location of jobs txt file stored
        String dataLocation = "game_data\\jobs.txt";

        // Create a new FileToStringConverter object
        FileToStringConverter fileToStringConverter = new FileToStringConverter(dataLocation);
        //Parse the text file into an ArrayList of String Arrays. Each String array consists of the object attributes.
        ArrayList<String[]> objectAttributes = fileToStringConverter.FileToString(21);

        int code = 1;
        /*public Job(int jobCode, JobType jobType, String jobName, double strength, double intelligence,
        double dexterity, double hp, double mp, double exp, double fighting, double short_blades,
        double long_blades, double axe, double mace_flail, double polearm, double staff, double unarmed,
        double spellcasting, String[] weaponChoice, String[] equipment, double skillPoints)*/

        for (String[] speciesAttr : objectAttributes) {
            Job job = new Job(code,
                    JobType.valueOf(speciesAttr[1]),
                    speciesAttr[0],
                    Double.parseDouble(speciesAttr[4]),
                    Double.parseDouble(speciesAttr[5]),
                    Double.parseDouble(speciesAttr[6]),
                    Double.parseDouble(speciesAttr[8]),
                    Double.parseDouble(speciesAttr[9]),
                    Double.parseDouble(speciesAttr[10]),
                    Double.parseDouble(speciesAttr[11]),
                    Double.parseDouble(speciesAttr[12]),
                    Double.parseDouble(speciesAttr[13]),
                    Double.parseDouble(speciesAttr[14]),
                    Double.parseDouble(speciesAttr[15]),
                    Double.parseDouble(speciesAttr[16]),
                    Double.parseDouble(speciesAttr[17]),
                    Double.parseDouble(speciesAttr[18]),
                    Double.parseDouble(speciesAttr[19]),
                    speciesAttr[2].split(","),
                    speciesAttr[3].split(","),
                    Double.parseDouble(speciesAttr[7]));
            gameDatabase.putJob(job);
            code++;
        }
    }

    /**
     * Loads species data from text file and creates Species objects
     */
    public void loadSpeciesTextData() {
        // Location of species txt file stored
        String dataLocation = "game_data\\species.txt";

        // Create a new FileToStringConverter object
        FileToStringConverter fileToStringConverter = new FileToStringConverter(dataLocation);
        //Parse the text file into an ArrayList of String Arrays. Each String array consists of the object attributes.
        ArrayList<String[]> objectAttributes = fileToStringConverter.FileToString(14);

        // After extracting the text data and converted to an array, add this as new objects.
        int code = 1;
        for (String[] speciesAttr : objectAttributes) {
            Species species = new Species(code,
                    SpeciesType.valueOf(speciesAttr[0]),
                    speciesAttr[1],
                    0.0,
                    0.0,
                    0.0,
                    Double.parseDouble(speciesAttr[2]),
                    Double.parseDouble(speciesAttr[3]),
                    Double.parseDouble(speciesAttr[4]),
                    Double.parseDouble(speciesAttr[5]),
                    Double.parseDouble(speciesAttr[6]),
                    Double.parseDouble(speciesAttr[7]),
                    Double.parseDouble(speciesAttr[8]),
                    Double.parseDouble(speciesAttr[9]),
                    Double.parseDouble(speciesAttr[10]),
                    Double.parseDouble(speciesAttr[11]),
                    Double.parseDouble(speciesAttr[12]),
                    Double.parseDouble(speciesAttr[13]));
            gameDatabase.putSpecies(species);
            code++;
        }
    }

    /**
     * A function to load logs
     */
    public void _loadFile() {
        String fileLocation = "game_log/out.txt";
    }

    /**
     * A function to write to logs
     */
    public void _writeFile() {
        System.out.println("Writing to log.txt ...");
        PrintWriter printWriter;
        try {
            printWriter = new PrintWriter(new FileOutputStream("game_log/out.txt"));

            for (int i = 1; i <= 10; i++) {
                printWriter.println(i);
            }

            System.out.println("Written to game_log/out.txt ...");

            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Gets the game database
     *
     * @return the game database
     */
    public GameDatabase getGameDatabase() {
        return gameDatabase;
    }

    /**
     * @return string representation of data loader
     */
    @Override
    public String toString() {
        return "DataLoader{" +
                "gameDatabase=" + gameDatabase +
                '}';
    }


}
