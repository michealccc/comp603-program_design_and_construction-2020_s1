package com.justanotherrpg.data.types;

/**
 * Representation of damage types
 *
 * @author 1079154 Micheal Chan
 */
public enum DamageType {
    UNKNOWN,
    PIERCING,
    SLICING,
    CHOPPING,
    SLASHING,
    CRUSHING,
}
