package com.justanotherrpg.data.types;

/**
 * Representation of species types
 *
 * @author 1079154 Micheal Chan
 */
public enum SpeciesType {
    SPECIES_UNKNOWN,
    SPECIES_HUMAN,
    SPECIES_ORC
}
