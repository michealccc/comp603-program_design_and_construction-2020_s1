package com.justanotherrpg.data.types;

/**
 * Representation of weapon types
 *
 * @author 1079154 Micheal Chan
 */
public enum WeaponType {
    UNKNOWN,
    SHORTBLADE,
    LONGBLADE,
    AXE,
    MACE_FLAIL,
    POLEARM,
    STAFF
}
