package com.justanotherrpg.data.types;

/**
 * Representation of job types
 *
 * @author 1079154 Micheal Chan
 */
public enum JobType {
    JOB_UNKNOWN,
    JOB_WARRIOR,
    JOB_MAGE
}
