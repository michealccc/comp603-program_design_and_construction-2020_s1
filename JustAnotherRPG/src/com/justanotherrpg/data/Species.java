package com.justanotherrpg.data;

import com.justanotherrpg.data.types.SpeciesType;

/**
 * Representation of a species
 *
 * @author 1079154 Micheal Chan
 */
public class Species extends Modifier {
    private int speciesCode;
    private SpeciesType speciesType;
    private String speciesName;

    public Species(int speciesCode, SpeciesType speciesType, String speciesName, double strength, double intelligence, double dexterity, double hp, double mp, double exp,
                   double fighting, double short_blades, double long_blades, double axe, double mace_flail, double polearm,
                   double staff, double unarmed, double spellcasting) {
        super(strength, intelligence, dexterity, hp, mp, exp, fighting, short_blades, long_blades, axe, mace_flail,
                polearm, staff, unarmed, spellcasting);
        this.speciesCode = speciesCode;
        this.speciesType = speciesType;
        this.speciesName = speciesName;
    }

    /**
     * Gets the species code
     *
     * @return species code
     */
    public int getSpeciesCode() {
        return speciesCode;
    }

    /**
     * Sets the species code
     *
     * @param speciesCode species code
     */
    public void setSpeciesCode(int speciesCode) {
        this.speciesCode = speciesCode;
    }

    /**
     * Gets the species type
     *
     * @return species type
     */
    public SpeciesType getSpeciesType() {
        return speciesType;
    }

    /**
     * Sets the species type
     *
     * @param speciesType species type
     */
    public void setSpeciesType(SpeciesType speciesType) {
        this.speciesType = speciesType;
    }

    /**
     * Gets the species name
     *
     * @return string value of species name
     */
    public String getSpeciesName() {
        return speciesName;
    }

    /**
     * Sets the species string name
     *
     * @param speciesName string name
     */
    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    /**
     * @return string representation of a species
     */
    @Override
    public String toString() {
        return "Species{" +
                "speciesCode=" + speciesCode +
                ", speciesType=" + speciesType +
                ", speciesName='" + speciesName + '\'' +
                '}';
    }
}
