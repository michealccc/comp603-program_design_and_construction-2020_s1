package com.justanotherrpg.data;

/**
 * Class that represents objects that can have modifier values to base stats
 *
 * @author 1079154 Micheal Chan
 */
public class Modifier {
    // Stats Modifiers
    private double strength;
    private double intelligence;
    private double dexterity;

    // General Modifiers
    private double hp;
    private double mp;
    private double exp;

    // Combat Modifiers
    private double fighting;
    private double short_blades;
    private double long_blades;
    private double axe;
    private double mace_flail;
    private double polearm;
    private double staff;
    private double unarmed;

    // Magic Modifiers
    private double spellcasting;

    public Modifier(double strength, double intelligence, double dexterity, double hp, double mp, double exp,
                    double fighting, double short_blades, double long_blades, double axe, double mace_flail, double polearm,
                    double staff, double unarmed, double spellcasting) {
        super();
        this.strength = strength;
        this.intelligence = intelligence;
        this.dexterity = dexterity;
        this.hp = hp;
        this.mp = mp;
        this.exp = exp;
        this.fighting = fighting;
        this.short_blades = short_blades;
        this.long_blades = long_blades;
        this.axe = axe;
        this.mace_flail = mace_flail;
        this.polearm = polearm;
        this.staff = staff;
        this.unarmed = unarmed;
        this.spellcasting = spellcasting;
    }

    /**
     * Gets the strength modifier value
     *
     * @return strength modifier value
     */
    public double getStrength() {
        return strength;
    }

    /**
     * Sets the strength modifier value
     *
     * @param strength strength modifier value
     */
    public void setStrength(double strength) {
        this.strength = strength;
    }

    /**
     * Gets the intelligence modifier value
     *
     * @return intelligence modifier value
     */
    public double getIntelligence() {
        return intelligence;
    }

    /**
     * Sets the strength modifier
     *
     * @param intelligence intelligence modifier value
     */
    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }

    /**
     * Gets the dexterity modifier value
     *
     * @return dexterity modifier value
     */
    public double getDexterity() {
        return dexterity;
    }

    /**
     * Sets the dexterity modifier value
     *
     * @param dexterity dexterity modifier value
     */
    public void setDexterity(double dexterity) {
        this.dexterity = dexterity;
    }

    /**
     * Gets the hp modifier value
     *
     * @return hp modifier value
     */
    public double getHp() {
        return hp;
    }

    /**
     * Sets the hp modifier value
     *
     * @param hp hp modifier value
     */
    public void setHp(double hp) {
        this.hp = hp;
    }

    /**
     * Gets the mp modifier value
     *
     * @return mp modifier value
     */
    public double getMp() {
        return mp;
    }

    /**
     * Sets the mp modifier value
     *
     * @param mp mp modifier value
     */
    public void setMp(double mp) {
        this.mp = mp;
    }

    /**
     * Gets the exp modifier value
     *
     * @return exp modifier value
     */
    public double getExp() {
        return exp;
    }

    /**
     * Sets the exp modifier value
     *
     * @param exp exp modifier value
     */
    public void setExp(double exp) {
        this.exp = exp;
    }

    /**
     * Gets the fighting modifier value
     *
     * @return fighting modifier value
     */
    public double getFighting() {
        return fighting;
    }

    /**
     * Sets the fighting modifier value
     *
     * @param fighting fighting modifier value
     */
    public void setFighting(double fighting) {
        this.fighting = fighting;
    }

    /**
     * Gets the short blades modifier value
     *
     * @return short blades modifier value
     */
    public double getShort_blades() {
        return short_blades;
    }

    /**
     * Sets the short blades modifier value
     *
     * @param short_blades short blades modifier value
     */
    public void setShort_blades(double short_blades) {
        this.short_blades = short_blades;
    }

    /**
     * Gets the long blades modifier value
     *
     * @return long blades modifier value
     */
    public double getLong_blades() {
        return long_blades;
    }

    /**
     * Sets the long blades modifier value
     *
     * @param long_blades long blades modifier value
     */
    public void setLong_blades(double long_blades) {
        this.long_blades = long_blades;
    }

    /**
     * Gets the axe modifier value
     *
     * @return axe modifier value
     */
    public double getAxe() {
        return axe;
    }

    /**
     * Sets the axe modifier value
     *
     * @param axe axe modifier value
     */
    public void setAxe(double axe) {
        this.axe = axe;
    }

    /**
     * Gets the mace/flail modifier value
     *
     * @return mace/flail modifier value
     */
    public double getMace_flail() {
        return mace_flail;
    }

    /**
     * Sets the mace/flail modifier value
     *
     * @param mace_flail mace/flail modifier value
     */
    public void setMace_flail(double mace_flail) {
        this.mace_flail = mace_flail;
    }

    /**
     * Gets the polearm modifier value
     *
     * @return polearm modifier value
     */
    public double getPolearm() {
        return polearm;
    }

    /**
     * Sets the polearm modifier value
     *
     * @param polearm polearm modifier value
     */
    public void setPolearm(double polearm) {
        this.polearm = polearm;
    }

    /**
     * Gets the staff modifier value
     *
     * @return staff modifier value
     */
    public double getStaff() {
        return staff;
    }

    /**
     * Sets the staff modifier value
     *
     * @param staff staff modifier value
     */
    public void setStaff(double staff) {
        this.staff = staff;
    }

    /**
     * Gets the unarmed modifier value
     *
     * @return unarmed modifier value
     */
    public double getUnarmed() {
        return unarmed;
    }

    /**
     * Sets the unarmed modifier value
     *
     * @param unarmed unarmed modifier value
     */
    public void setUnarmed(double unarmed) {
        this.unarmed = unarmed;
    }

    /**
     * Gets the spellcasting modifier value
     *
     * @return spellcasting modifier value
     */
    public double getSpellcasting() {
        return spellcasting;
    }

    /**
     * Sets the spellcasting modifier value
     *
     * @param spellcasting spellcasting modifier value
     */
    public void setSpellcasting(double spellcasting) {
        this.spellcasting = spellcasting;
    }

    /**
     * @return string representation of modifier
     */
    @Override
    public String toString() {
        return "Modifier{" +
                "strength=" + strength +
                ", intelligence=" + intelligence +
                ", dexterity=" + dexterity +
                ", hp=" + hp +
                ", mp=" + mp +
                ", exp=" + exp +
                ", fighting=" + fighting +
                ", short_blades=" + short_blades +
                ", long_blades=" + long_blades +
                ", axe=" + axe +
                ", mace_flail=" + mace_flail +
                ", polearm=" + polearm +
                ", staff=" + staff +
                ", unarmed=" + unarmed +
                ", spellcasting=" + spellcasting +
                '}';
    }
}
