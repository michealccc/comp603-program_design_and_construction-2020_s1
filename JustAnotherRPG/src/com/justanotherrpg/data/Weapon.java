package com.justanotherrpg.data;

import com.justanotherrpg.data.types.DamageType;
import com.justanotherrpg.data.types.WeaponType;

/**
 * Representation of a weapon
 *
 * @author 1079154 Micheal Chan
 */
public class Weapon {
    private int weaponCode;
    private String weaponName;
    private WeaponType weaponType;
    private DamageType weaponDamageType;

    private double weaponDamage;
    private double weaponHit;
    private double weaponSpeed;

    private boolean isTwoHanded;

    public Weapon(int weaponCode, String weaponName, WeaponType weaponType, DamageType weaponDamageType, double weaponDamage, double weaponHit, double weaponSpeed, boolean isTwoHanded) {
        this.weaponCode = weaponCode;
        this.weaponName = weaponName;
        this.weaponType = weaponType;
        this.weaponDamageType = weaponDamageType;
        this.weaponDamage = weaponDamage;
        this.weaponHit = weaponHit;
        this.weaponSpeed = weaponSpeed;
        this.isTwoHanded = isTwoHanded;
    }

    /**
     * Gets the weapon code
     *
     * @return the weapon code
     */
    public int getWeaponCode() {
        return weaponCode;
    }

    /**
     * Sets the weapon code
     *
     * @param weaponCode a weapon code
     */
    public void setWeaponCode(int weaponCode) {
        this.weaponCode = weaponCode;
    }

    /**
     * Gets the string name value for the weapon
     *
     * @return string name value of weapon
     */
    public String getWeaponName() {
        return weaponName;
    }

    /**
     * Sets the string name value for a weapon's name
     *
     * @param weaponName a string value for weapon name
     */
    public void setWeaponName(String weaponName) {
        this.weaponName = weaponName;
    }

    /**
     * Gets the weapon type
     *
     * @return the weapon type
     */
    public WeaponType getWeaponType() {
        return weaponType;
    }

    /**
     * Sets the weapon type
     *
     * @param weaponType a weapon type
     */
    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    /**
     * Gets the weapon damage type
     *
     * @return the weapon damage type
     */
    public DamageType getWeaponDamageType() {
        return weaponDamageType;
    }

    /**
     * Sets the weapon damage type
     *
     * @param weaponDamageType a weapon damage type
     */
    public void setWeaponDamageType(DamageType weaponDamageType) {
        this.weaponDamageType = weaponDamageType;
    }

    /**
     * Gets the weapon damage value
     *
     * @return weapon damage value
     */
    public double getWeaponDamage() {
        return weaponDamage;
    }

    /**
     * Sets the weapon damage value
     *
     * @param weaponDamage a weapon damage value
     */
    public void setWeaponDamage(double weaponDamage) {
        this.weaponDamage = weaponDamage;
    }

    /**
     * Gets the weapon hit value
     *
     * @return the weapon hit value
     */
    public double getWeaponHit() {
        return weaponHit;
    }

    /**
     * Sets the weapon hit value
     *
     * @param weaponHit a weapon hit value
     */
    public void setWeaponHit(double weaponHit) {
        this.weaponHit = weaponHit;
    }

    /**
     * Gets the weapon speed value
     *
     * @return the weapon speed value
     */
    public double getWeaponSpeed() {
        return weaponSpeed;
    }

    /**
     * Sets the weapon speed value
     *
     * @param weaponSpeed a weapon speed value
     */
    public void setWeaponSpeed(double weaponSpeed) {
        this.weaponSpeed = weaponSpeed;
    }

    /**
     * Gets the boolean value for two handed weapon
     *
     * @return true: if a weapon is a two-handed weapon. | false: if a weapon is not a two-handed weapon.
     */
    public boolean isTwoHanded() {
        return isTwoHanded;
    }

    /**
     * Sets the boolean value for two handed weapon
     *
     * @param twoHanded true: if a weapon is a two-handed weapon. | false: if a weapon is not a two-handed weapon.
     */
    public void setTwoHanded(boolean twoHanded) {
        isTwoHanded = twoHanded;
    }

    /**
     * @return string representation of a weapon
     */
    @Override
    public String toString() {
        return "Weapon{" +
                "weaponName='" + weaponName + '\'' +
                ", weaponType=" + weaponType +
                ", weaponDamageType=" + weaponDamageType +
                ", weaponDamage=" + weaponDamage +
                ", weaponHit=" + weaponHit +
                ", weaponSpeed=" + weaponSpeed +
                ", isTwoHanded=" + isTwoHanded +
                '}';
    }
}
