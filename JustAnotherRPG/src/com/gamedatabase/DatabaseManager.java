package com.gamedatabase;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class manages all database connections and SQL queries to store Player stats of
 * how many games have been won or lost.
 *
 * @author 1079154 Micheal Chan
 */
public class DatabaseManager {

    private static final String url = "jdbc:derby:gameDatabase;create=true";
    private static final String username = "justanotherrpg";
    private static final String password = "PMvuk6LhzE2fRnMrKNrw";
    private static final String tableName = "PLAYERSTATS";
    Statement statement;
    ResultSet resultSet;
    private Connection connection;
    private DatabaseManager dbm;

    public DatabaseManager() {
        establishConnection();
    }

    public Connection getConnection() {
        return this.connection;
    }

    // Establish connection
    public void establishConnection() {
        if (this.connection == null) {
            try {
                this.connection = DriverManager.getConnection(url, username, password);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Create table to store PlayerStats and insert sample data, as required
     */
    public void playerStatsSetup() {
        dbm = new DatabaseManager();
        this.createPlayerStatsTable();
        this.insertPlayerStatsSampleData();
    }

    /**
     * Creates the PLAYERSTATS table into the database if it does not already exist
     */
    private void createPlayerStatsTable() {
        try {
            Statement statement = dbm.getConnection().createStatement();
            DatabaseMetaData databaseMetadata = dbm.getConnection().getMetaData();
            ResultSet resultSet = databaseMetadata.getTables(null, null, tableName, null);

            // If table does not already exist, create a new one.
            if (!resultSet.next()) {
                statement.executeUpdate("CREATE TABLE " + tableName +
                        " (game_ended TIMESTAMP, result INTEGER)");
            }
        } catch (SQLException ex) {
            System.err.println("Could not create Table: " + ex);
        }
    }

    /**
     * If the PLAYERSTATS table is empty, insert sample player statistics data into it.
     */
    private void insertPlayerStatsSampleData() {
        try {
            Statement statement = this.connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM " + tableName);

            // If table is empty, create sample data
            if (!resultSet.next()) {

                Timestamp gameEndTimestamp = new Timestamp(System.currentTimeMillis());
                PreparedStatement ps = connection.prepareStatement("INSERT INTO " + tableName +
                        "(game_ended, result) VALUES (?, ?)");

                // Create 4 rows of sample stats data, recorded as game losses
                for (int i = 0; i < 4; i++) {
                    ps.setObject(1, gameEndTimestamp);
                    ps.setInt(2, 0);
                    ps.executeUpdate();
                    connection.commit();
                }

                // Create 3 rows of sample stats data, recorded as game wins
                for (int i = 0; i < 3; i++) {
                    ps.setObject(1, gameEndTimestamp);
                    ps.setInt(2, 1);
                    ps.executeUpdate();
                    connection.commit();
                }
            }
        } catch (SQLException ex) {
            System.err.println("Could not insert player stats: " + ex);
        }
    }

    /**
     * Inserts a record into the PLAYERSTATS table when the player has lost or not completed a game
     */
    public void insertGameLossRecord() {
        try {
            Statement statement = this.connection.createStatement();

            Timestamp gameEndTimestamp = new Timestamp(System.currentTimeMillis());
            PreparedStatement ps = connection.prepareStatement("INSERT INTO " + tableName +
                    "(game_ended, result) VALUES (?, ?)");
            ps.setObject(1, gameEndTimestamp);
            ps.setInt(2, 0);
            ps.executeUpdate();
            connection.commit();
        } catch (SQLException ex) {
            System.err.println("Could not insert game loss record: " + ex);
        }
    }

    /**
     * Returns the number of of games that the player has won
     *
     * @return An integer representing number of games won
     */
    public int totalWins() {
        int nWins = 0;
        try {
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT COUNT(*) FROM PLAYERSTATS WHERE RESULT = 1");
            while (rs.next()) {
                nWins = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.err.println("Could not perform SELECT statement for totalWins: " + ex);
        }
        return nWins;
    }

    /**
     * Returns the number of of games that the player has lost
     *
     * @return An integer representing number of games lost
     */
    public int totalLosses() {
        int nLosses = 0;
        try {
            Statement statement = this.connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT COUNT(*) FROM PLAYERSTATS WHERE RESULT = 0");
            while (rs.next()) {
                nLosses = rs.getInt(1);
            }
        } catch (SQLException ex) {
            System.err.println("Could not perform SELECT statement for totalLosses: " + ex);
        }
        return nLosses;
    }
}
